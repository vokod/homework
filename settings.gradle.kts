pluginManagement {
    @Suppress("UnstableApiUsage")
    includeBuild("build-logic")
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}

dependencyResolutionManagement {
    @Suppress("UnstableApiUsage")
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    @Suppress("UnstableApiUsage")
    repositories {
        google()
        mavenCentral()
        maven {url= java.net.URI.create("https://jitpack.io") }
    }
}

rootProject.name = "Homework"

include(":app")
include(":core:common")
include(":core:designsystem")
include(":core:domain")
include(":core:network")
include(":core:model")
include(":core:preferences")
include(":core:repository")

include(":feature:currentweather")
include(":feature:forecast")
include(":feature:locations")

include(":screenshottest")
