@file:Suppress("UnstableApiUsage")


plugins {
    id("homework.android.application")
    id("homework.android.application.compose")
}

android {
    buildFeatures { buildConfig = true }

    defaultConfig {
        applicationId = "com.vokod.homework"

        versionCode = 1
        versionName = "1"

        testApplicationId = "com.vokod.homework.test"

        vectorDrawables { useSupportLibrary = true }
    }

    buildTypes {
        val release by getting {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }

        val debug by getting { isDebuggable = true }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    testOptions {
        unitTests.isReturnDefaultValues = true
        unitTests.isIncludeAndroidResources = true
    }

    testOptions {
        unitTests { isIncludeAndroidResources = true }
    }

    namespace = "com.vokod.homework"

    kotlinOptions {
        jvmTarget = "11"
        allWarningsAsErrors = false

        freeCompilerArgs = freeCompilerArgs + "-Xjvm-default=all-compatibility"
        freeCompilerArgs = freeCompilerArgs + "-opt-in=kotlin.RequiresOptIn"
        freeCompilerArgs =
            freeCompilerArgs + "-opt-in=androidx.compose.foundation.ExperimentalFoundationApi"
    }

}

dependencies {
    implementation(project(":core:common"))
    implementation(project(":core:designsystem"))
    implementation(project(":core:domain"))
    implementation(project(":core:preferences"))

    implementation(project(":feature:currentweather"))
    implementation(project(":feature:forecast"))
    implementation(project(":feature:locations"))

    implementation(libs.accompanist.navigation.animation)
    implementation(libs.accompanist.systemuicontroller)
    implementation(libs.androidx.activity.compose)
    implementation(libs.androidx.lifecycle.runtimeCompose)
    implementation(libs.androidx.compose.runtime)
    implementation(libs.androidx.compose.material3.windowSizeClass)
    implementation(libs.androidx.compose.foundation)
    implementation(libs.androidx.compose.material3)
    implementation(libs.androidx.navigation.compose)
    implementation(libs.koin.android)
    implementation(libs.koin.compose)
    implementation(libs.ouroboros)
    implementation(libs.timber)

    testImplementation(libs.koin.test)

    androidTestImplementation(kotlin("test"))

}

// androidx.test is forcing JUnit, 4.12. This forces it to use 4.13
configurations.configureEach {
    resolutionStrategy {
        force(libs.junit4)
        // Temporary workaround for https://issuetracker.google.com/174733673
        force("org.objenesis:objenesis:3.3")
    }
}