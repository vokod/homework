package com.vokod.homework.di

import org.junit.Test
import org.koin.test.verify.verify

@Suppress("OPT_IN_USAGE")
class AppModuleCheck {

    @Test
    fun checkKoinModule() {
        appModule.verify()
    }
}