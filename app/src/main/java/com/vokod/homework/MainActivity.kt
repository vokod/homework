package com.vokod.homework

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.LaunchedEffect
import androidx.core.view.WindowCompat
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.vokod.homework.core.designsystem.theme.HomeworkTheme
import com.vokod.homework.ui.HomeworkApp

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            val systemUiController = rememberSystemUiController()
            val useDarkTheme = isSystemInDarkTheme()

            LaunchedEffect(systemUiController, useDarkTheme) {
                systemUiController.systemBarsDarkContentEnabled = !useDarkTheme
            }

            HomeworkTheme(useDarkTheme = useDarkTheme) { HomeworkApp() }
        }
    }
}
