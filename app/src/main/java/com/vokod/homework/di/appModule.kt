package com.vokod.homework.di

import com.vokod.homework.feature.currentweather.di.currentWeatherModule
import com.vokod.homework.feature.forecast.di.forecastModule
import com.vokod.homework.feature.locations.di.locationsModule
import org.koin.dsl.module

val appModule = module {
    includes(currentWeatherModule, locationsModule, forecastModule)
}
