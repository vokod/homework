package com.vokod.homework.ui

import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.WindowInsetsSides
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.only
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawing
import androidx.compose.foundation.layout.windowInsetsPadding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.vokod.homework.HomeworkNavHost

@OptIn(
    ExperimentalMaterial3Api::class,
    ExperimentalLayoutApi::class
)
@Composable
fun HomeworkApp(appState: HomeworkAppState = rememberHomeworkAppState()) {

    Scaffold(contentWindowInsets = WindowInsets(0, 0, 0, 0)) { padding ->
        val insets = WindowInsets.safeDrawing.run {
            this.only(WindowInsetsSides.Horizontal)
        }
        Row(
            Modifier
                .fillMaxSize()
                .windowInsetsPadding(insets)
        ) {
            HomeworkNavHost(
                navController = appState.navController,
                navigateBack = { appState.navController.popBackStack() },
                modifier = Modifier
                    .padding(padding)
                    .consumeWindowInsets(padding)
            )
        }
    }
}
