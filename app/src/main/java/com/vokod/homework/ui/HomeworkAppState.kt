package com.vokod.homework.ui

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import com.google.accompanist.navigation.animation.rememberAnimatedNavController

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun rememberHomeworkAppState(
    navController: NavHostController = rememberAnimatedNavController(),
): HomeworkAppState {
    return remember(navController) { HomeworkAppState(navController = navController) }
}

@Stable
data class HomeworkAppState(val navController: NavHostController)
