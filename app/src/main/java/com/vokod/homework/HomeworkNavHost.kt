package com.vokod.homework

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.vokod.homework.core.common.NavigationRoutes
import com.vokod.homework.feature.currentweather.navigation.currentWeatherNavigation
import com.vokod.homework.feature.forecast.navigation.forecastNavigation
import com.vokod.homework.feature.forecast.navigation.navigateToForecast
import com.vokod.homework.feature.locations.navigation.locationsNavigation
import com.vokod.homework.feature.locations.navigation.navigateToLocations


@OptIn(ExperimentalAnimationApi::class)
@Composable
fun HomeworkNavHost(
    navController: NavHostController,
    navigateBack: () -> Unit,
    modifier: Modifier = Modifier,
    startDestination: String = NavigationRoutes.CURRENT
) {
    AnimatedNavHost(
        navController = navController,
        startDestination = startDestination,
        modifier = modifier,
    ) {
        currentWeatherNavigation(
            navigateToForecast = { navController.navigateToForecast() },
            navigateToLocations = { navController.navigateToLocations() })

        locationsNavigation(navigateBack)

        forecastNavigation(navigateBack)
    }
}
