import com.android.build.gradle.LibraryExtension
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure

class AndroidFeatureConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            pluginManager.apply { apply("homework.android.library") }
            extensions.configure<LibraryExtension> {

                @Suppress("UnstableApiUsage")
                testOptions {
                    unitTests.apply {
                        isIncludeAndroidResources = true
                        all { it.allJvmArgs = it.allJvmArgs + listOf("-Xmx4192M") }
                    }
                }
            }
        }
    }
}
