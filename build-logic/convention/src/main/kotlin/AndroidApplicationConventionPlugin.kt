import com.android.build.api.dsl.ApplicationExtension
import com.vokod.homework.configureKotlinAndroid
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure

class AndroidApplicationConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            with(pluginManager) {
                apply("com.android.application")
                apply("org.jetbrains.kotlin.android")
            }

            extensions.configure<ApplicationExtension> {
                configureKotlinAndroid(this)
                defaultConfig.targetSdk = 33
                defaultConfig.testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
                defaultConfig.testApplicationId = "com.vokod.homework.test"

                @Suppress("UnstableApiUsage")
                testOptions { unitTests.apply { isIncludeAndroidResources = true } }
            }
        }
    }
}
