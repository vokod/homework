package com.vokod.homework.feature.locations.ouroboros

import com.bridge.ouroboros.compose.test.matches
import com.bridge.ouroboros.compose.test.receives
import com.vokod.homework.core.domain.model.UserLocations
import org.junit.Test

class LocationsEventTest {

    private val defaultModel = LocationsModel(
        data = Result.success(
            UserLocations(
                locations = setOf("a", "b"),
                defaultLocation = "a"
            )
        )
    )

    @Test
    fun `RetryClicked event`() {
        defaultModel receives LocationsEvent.RetryClicked matches {
            shouldHaveModel(defaultModel.copy(data = null))
            shouldHaveEffects(LocationsEffect.FetchUserLocations)
        }
    }

    @Test
    fun `AddLocationButtonClicked event`() {
        defaultModel receives LocationsEvent.AddLocationButtonClicked matches {
            shouldHaveModel(defaultModel.copy(showAddLocationDialog = true))
            shouldNotHaveEffects()
        }
    }

    @Test
    fun `AddLocationDialogDismissed event`() {
        val initModel = defaultModel.copy(showAddLocationDialog = true)
        initModel receives LocationsEvent.AddLocationDialogDismissed matches {
            shouldHaveModel(initModel.copy(showAddLocationDialog = false))
            shouldNotHaveEffects()
        }
    }

    @Test
    fun `AddLocationDialogValueChanged event`() {
        val initModel = defaultModel.copy(showAddLocationDialog = true)
        initModel receives LocationsEvent.AddLocationDialogValueChanged("c") matches {
            shouldHaveModel(initModel.copy(addLocationDialogValue = "c"))
            shouldNotHaveEffects()
        }
    }

    @Test
    fun `AddLocationDialogConcluded event`() {
        val initModel =
            defaultModel.copy(showAddLocationDialog = true, addLocationDialogValue = "c")
        initModel receives LocationsEvent.AddLocationDialogConcluded matches {
            shouldHaveModel(
                initModel.copy(
                    showAddLocationDialog = false,
                    addLocationDialogValue = ""
                )
            )
            shouldHaveEffects(LocationsEffect.SaveLocation("c"))
        }
    }

    @Test
    fun `AddLocationDialogConcluded event when first location`() {
        val initModel = defaultModel.copy(
            showAddLocationDialog = true,
            data = Result.success(UserLocations(emptySet(), null)),
            addLocationDialogValue = "c"
        )
        initModel receives LocationsEvent.AddLocationDialogConcluded matches {
            shouldHaveModel(
                initModel.copy(
                    showAddLocationDialog = false,
                    addLocationDialogValue = ""
                )
            )
            shouldHaveEffects(
                LocationsEffect.SaveLocation("c"),
                LocationsEffect.SetDefaultLocation("c")
            )
        }
    }

    @Test
    fun `UserLocationsFetched event`() {
        val initModel = LocationsModel()
        initModel receives LocationsEvent.UserLocationsFetched(
            Result.success(
                UserLocations(
                    locations = setOf("a", "b"),
                    defaultLocation = "a"
                )
            )
        ) matches {
            shouldHaveModel(defaultModel)
            shouldNotHaveEffects()
        }
    }

    @Test
    fun `DefaultLocationSelected event`() {
        defaultModel receives LocationsEvent.DefaultLocationSelected("a") matches {
            shouldNotHaveModel()
            shouldHaveEffects(LocationsEffect.SetDefaultLocation("a"))
        }
    }

    @Test
    fun `LocationLongClicked event`() {
        defaultModel receives LocationsEvent.LocationLongClicked("a") matches {
            shouldHaveModel(defaultModel.copy(locationToRemove = "a"))
            shouldNotHaveEffects()
        }
    }

    @Test
    fun `RemoveLocationDialogDismissed event`() {
        val initModel = defaultModel.copy(locationToRemove = "a")
        initModel receives LocationsEvent.RemoveLocationDialogDismissed matches {
            shouldHaveModel(defaultModel)
            shouldNotHaveEffects()
        }
    }

    @Test
    fun `RemoveLocationRequested event`() {
        val initModel = defaultModel.copy(locationToRemove = "a")
        initModel receives LocationsEvent.RemoveLocationRequested matches {
            shouldHaveModel(defaultModel)
            shouldHaveEffects(LocationsEffect.RemoveLocation("a"))
        }
    }
}