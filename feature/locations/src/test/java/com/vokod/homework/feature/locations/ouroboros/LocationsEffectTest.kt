package com.vokod.homework.feature.locations.ouroboros

import com.bridge.ouroboros.compose.test.matches
import com.bridge.ouroboros.compose.test.runWith
import com.vokod.homework.core.domain.model.UserLocations
import com.vokod.homework.core.preferences.HomeworkPreferences
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.impl.annotations.MockK
import io.mockk.just
import io.mockk.runs
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

class LocationsEffectTest {

    @MockK
    lateinit var preferences: HomeworkPreferences

    lateinit var state: LocationsEffect.State

    @Before
    fun before() {
        MockKAnnotations.init(this)
        state = LocationsEffect.State(preferences)
    }

    @Test
    fun `SaveLocation effect`() {
        runTest {
            coEvery { preferences.addLocation(any()) } just runs

            LocationsEffect.SaveLocation("a") runWith state matches { expectNoEvents() }

            coVerify { preferences.addLocation("a") }

            confirmVerified(preferences)
        }
    }

    @Test
    fun `RemoveLocation effect`() {
        runTest {
            coEvery { preferences.removeLocation(any()) } just runs

            LocationsEffect.RemoveLocation("a") runWith state matches { expectNoEvents() }

            coVerify { preferences.removeLocation("a") }

            confirmVerified(preferences)
        }
    }

    @Test
    fun `SetDefaultLocation effect`() {
        runTest {
            coEvery { preferences.setDefaultLocation(any()) } just runs

            LocationsEffect.SetDefaultLocation("a") runWith state matches { expectNoEvents() }

            coVerify { preferences.setDefaultLocation("a") }

            confirmVerified(preferences)
        }
    }

    @Test
    fun `FetchUserLocations effect`() {
        runTest {
            coEvery { preferences.getUserLocations() } returns
                    flow {
                        emit(UserLocations(emptySet(), null))
                        emit(UserLocations(setOf("a"), "a"))
                    }

            LocationsEffect.FetchUserLocations runWith state matches {
                expectEvents(
                    LocationsEvent.UserLocationsFetched(
                        Result.success(UserLocations(emptySet(), null))
                    ),
                    LocationsEvent.UserLocationsFetched(
                        Result.success(UserLocations(setOf("a"), "a"))
                    )
                )
            }

            coVerify { preferences.getUserLocations() }

            confirmVerified(preferences)
        }
    }
}
