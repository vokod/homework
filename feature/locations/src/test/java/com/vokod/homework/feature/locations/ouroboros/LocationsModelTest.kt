package com.vokod.homework.feature.locations.ouroboros

import io.mockk.mockk
import org.junit.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class LocationsModelTest {

    private val defaultModel = LocationsModel()

    @Test
    fun showLoading() {
        assertTrue(defaultModel.showLoading)
    }

    @Test
    fun showLoading_not() {
        assertFalse(defaultModel.copy(data = Result.failure(Exception())).showLoading)
    }

    @Test
    fun showLoading_not_2() {
        assertFalse(defaultModel.copy(data = Result.success(mockk())).showLoading)
    }

    @Test
    fun showError() {
        assertFalse(defaultModel.showError)
    }

    @Test
    fun showError_2() {
        assertTrue(defaultModel.copy(data = Result.failure(Exception())).showError)
    }

    @Test
    fun showError_3() {
        assertFalse(defaultModel.copy(data = Result.success(mockk())).showError)
    }
}