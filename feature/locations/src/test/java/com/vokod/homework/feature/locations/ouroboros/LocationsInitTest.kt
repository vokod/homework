package com.vokod.homework.feature.locations.ouroboros

import com.bridge.ouroboros.compose.test.matches
import org.junit.Test

class LocationsInitTest {

    @Test
    fun init() {
        locationsInit()() matches {
            shouldHaveModel(LocationsModel())
            shouldHaveEffects(LocationsEffect.FetchUserLocations)
        }
    }
}
