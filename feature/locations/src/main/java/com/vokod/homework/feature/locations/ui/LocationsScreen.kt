package com.vokod.homework.feature.locations.ui

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FabPosition
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.bridge.ouroboros.compose.EventConsumer
import com.vokod.homework.core.common.R
import com.vokod.homework.core.designsystem.component.HomeworkTopAppBar
import com.vokod.homework.core.designsystem.component.dialog.ConfirmationDialog
import com.vokod.homework.core.designsystem.component.dialog.TextEnterDialog
import com.vokod.homework.core.designsystem.component.page.FullscreenErrorState
import com.vokod.homework.core.designsystem.component.page.FullscreenLoadingState
import com.vokod.homework.core.designsystem.component.page.ReadableWidthContainer
import com.vokod.homework.core.designsystem.icon.HomeworkIcons
import com.vokod.homework.feature.locations.ouroboros.LocationsEvent
import com.vokod.homework.feature.locations.ouroboros.LocationsModel

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun LocationsScreen(
    model: LocationsModel,
    dispatchEvent: EventConsumer<LocationsEvent>,
    navigateBack: () -> Unit,
) {
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(rememberTopAppBarState())


    if (model.showAddLocationDialog) {
        TextEnterDialog(
            value = model.addLocationDialogValue,
            label = stringResource(R.string.add_location),
            placeholder = stringResource(R.string.enter_a_name),
            onDismissRequest = { dispatchEvent(LocationsEvent.AddLocationDialogDismissed) },
            onConfirmClick = { dispatchEvent(LocationsEvent.AddLocationDialogConcluded) },
            onValueChange = { dispatchEvent(LocationsEvent.AddLocationDialogValueChanged(it)) }
        )
    }

    if (model.locationToRemove != null) {
        ConfirmationDialog(
            title = stringResource(R.string.wait),
            message = stringResource(
                id = R.string.are_you_sure_you_want_to_,
                model.locationToRemove
            ),
            onConfirmButtonClick = { dispatchEvent(LocationsEvent.RemoveLocationRequested) },
            onDismissButtonClick = { dispatchEvent(LocationsEvent.RemoveLocationDialogDismissed) }
        )
    }

    Scaffold(
        topBar = {
            HomeworkTopAppBar(
                title = {
                    Text(
                        text = stringResource(R.string.locations),
                        maxLines = 1,
                        overflow = TextOverflow.Visible
                    )
                },
                scrollBehavior = scrollBehavior,
                navigationIcon = HomeworkIcons.BackArrow,
                onNavigationClick = navigateBack,
                navigationIconContentDescription = stringResource(id = R.string.go_back)
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = { dispatchEvent(LocationsEvent.AddLocationButtonClicked) }
            ) {
                Icon(
                    painter = HomeworkIcons.Plus.painter,
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.onSurface
                )
            }
        },
        floatingActionButtonPosition = FabPosition.End,
        modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
    )
    { paddingValues ->
        ReadableWidthContainer(
            modifier = Modifier.padding(top = paddingValues.calculateTopPadding())
        ) {
            when {
                model.showLoading -> FullscreenLoadingState()
                model.showError ->
                    FullscreenErrorState { dispatchEvent(LocationsEvent.RetryClicked) }
                else -> {
                    model.locations?.let { locationsSet ->
                        val locations = locationsSet.toList()
                        if (locations.isNotEmpty()) {
                            LazyColumn(
                                modifier = Modifier.fillMaxSize(),
                                contentPadding = PaddingValues(24.dp),
                                verticalArrangement = Arrangement.spacedBy(24.dp)
                            ) {
                                items(count = locations.size, key = { locations[it] }) { index ->
                                    val location = locations[index]
                                    LocationItem(
                                        location = location,
                                        isDefault = model.defaultLocation == location,
                                        modifier = Modifier.animateItemPlacement(),
                                        onClick = {
                                            dispatchEvent(
                                                LocationsEvent.DefaultLocationSelected(location)
                                            )
                                        },
                                        onLongClick = {
                                            dispatchEvent(
                                                LocationsEvent.LocationLongClicked(location)
                                            )
                                        }
                                    )
                                }
                            }
                        } else {
                            Box(
                                modifier = Modifier.fillMaxSize(),
                                contentAlignment = Alignment.Center
                            ) {
                                Text(
                                    modifier = Modifier.padding(48.dp),
                                    style = MaterialTheme.typography.titleSmall,
                                    text = stringResource(R.string.it_seems_you_have_no_),
                                    textAlign = TextAlign.Center
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun LocationItem(
    location: String,
    isDefault: Boolean,
    onClick: () -> Unit,
    onLongClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    val defaultIconAlpha: Float by animateFloatAsState(if (isDefault) 1f else 0f)

    Card(
        colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.secondaryContainer),
        shape = RoundedCornerShape(16.dp),
        modifier = Modifier
            .clip(RoundedCornerShape(16.dp))
            .combinedClickable(
                onClick = onClick,
                onLongClick = onLongClick
            )
            .then(modifier),
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(vertical = 8.dp, horizontal = 16.dp)
        ) {

            Icon(
                painter = HomeworkIcons.Location.painter,
                contentDescription = null,
                tint = MaterialTheme.colorScheme.secondary
            )

            Spacer(modifier = Modifier.size(16.dp))

            Text(
                text = location,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.titleSmall
            )

            Spacer(
                modifier = Modifier
                    .size(16.dp)
                    .weight(1f)
            )

            Surface(
                modifier = Modifier
                    .size(40.dp)
                    .graphicsLayer(alpha = defaultIconAlpha),
                shape = RoundedCornerShape(20.dp),
                color = MaterialTheme.colorScheme.primaryContainer
            ) {
                Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                    Icon(
                        modifier = Modifier.size(24.dp),
                        painter = HomeworkIcons.Check.painter,
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.primary
                    )
                }
            }
        }
    }
}
