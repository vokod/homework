package com.vokod.homework.feature.locations.ouroboros

import com.bridge.ouroboros.compose.EventConsumer
import com.bridge.ouroboros.compose.ExecutableEffect
import com.vokod.homework.core.common.LongRunningCoroutine
import com.vokod.homework.core.common.resultOf
import com.vokod.homework.core.preferences.HomeworkPreferences
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

sealed class LocationsEffect :
    ExecutableEffect<LocationsEvent, LocationsEffect.State>() {

    data class SaveLocation(val location: String) : LocationsEffect() {
        override fun State.perform(emit: EventConsumer<LocationsEvent>) {
            launch { preferences.addLocation(location) }
        }
    }

    data class RemoveLocation(val location: String) : LocationsEffect() {
        override fun State.perform(emit: EventConsumer<LocationsEvent>) {
            launch { preferences.removeLocation(location) }
        }
    }

    data class SetDefaultLocation(val location: String) : LocationsEffect() {
        override fun State.perform(emit: EventConsumer<LocationsEvent>) {
            launch { preferences.setDefaultLocation(location) }
        }
    }

    object FetchUserLocations : LocationsEffect() {
        override fun State.perform(emit: EventConsumer<LocationsEvent>) {
            launch(LongRunningCoroutine) {
                preferences.getUserLocations().collect {
                    emit(LocationsEvent.UserLocationsFetched(resultOf { it }))
                }
            }
        }
    }

    class State(preferences: HomeworkPreferences? = null) : KoinComponent {
        val preferences: HomeworkPreferences = preferences ?: get()
    }
}
