package com.vokod.homework.feature.locations.ui

import androidx.compose.runtime.Composable
import com.vokod.homework.core.designsystem.HomeworkPreview
import com.vokod.homework.core.designsystem.ScreenPreview
import com.vokod.homework.core.domain.model.UserLocations
import com.vokod.homework.feature.locations.ouroboros.LocationsModel


@ScreenPreview
@Composable
fun LocationsScreenLoadingPreview() {
    HomeworkPreview {
        LocationsScreen(
            model = LocationsModel(),
            dispatchEvent = {},
            navigateBack = {},
        )
    }
}


@ScreenPreview
@Composable
fun LocationsScreenErrorPreview() {
    HomeworkPreview {
        LocationsScreen(
            model = LocationsModel(data = Result.failure(Exception())),
            dispatchEvent = {},
            navigateBack = {},
        )
    }
}

@ScreenPreview
@Composable
fun LocationsScreenContentPreview() {
    HomeworkPreview {
        LocationsScreen(
            model = LocationsModel(
                data = Result.success(
                    UserLocations(
                        locations = setOf(
                            "London",
                            "Paris",
                            "New York",
                            "Karakószörcsög",
                            "Hajós"
                        ),
                        defaultLocation = "Hajós"
                    )
                )
            ),
            dispatchEvent = {},
            navigateBack = {},
        )
    }
}

@ScreenPreview
@Composable
fun LocationsScreenContentPreview_addLocationDialog() {
    HomeworkPreview {
        LocationsScreen(
            model = LocationsModel(
                data = Result.success(
                    UserLocations(
                        locations = setOf(
                            "London",
                            "Paris",
                            "New York",
                            "Karakószörcsög",
                            "Hajós"
                        ),
                        defaultLocation = "Hajós"
                    )
                ),
                showAddLocationDialog = true
            ),
            dispatchEvent = {},
            navigateBack = {},
        )
    }
}

@ScreenPreview
@Composable
fun LocationsScreenContentPreview_removeLocationDialog() {
    HomeworkPreview {
        LocationsScreen(
            model = LocationsModel(
                data = Result.success(
                    UserLocations(
                        locations = setOf(
                            "London",
                            "Paris",
                            "New York",
                            "Karakószörcsög",
                            "Hajós"
                        ),
                        defaultLocation = "Hajós"
                    )
                ),
                locationToRemove = "Karakószörcsög"
            ),
            dispatchEvent = {},
            navigateBack = {},
        )
    }
}
