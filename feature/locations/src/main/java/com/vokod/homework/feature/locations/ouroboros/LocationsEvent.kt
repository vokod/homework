package com.vokod.homework.feature.locations.ouroboros

import com.bridge.ouroboros.compose.ActionableEvent
import com.bridge.ouroboros.compose.Next
import com.vokod.homework.core.domain.model.UserLocations


sealed class LocationsEvent : ActionableEvent<LocationsModel, LocationsEffect> {

    object RetryClicked : LocationsEvent() {
        override fun perform(model: LocationsModel) = change(
            model.copy(data = null),
            LocationsEffect.FetchUserLocations
        )
    }

    object AddLocationButtonClicked : LocationsEvent() {
        override fun perform(model: LocationsModel) = change(
            model.copy(showAddLocationDialog = true)
        )
    }

    data class AddLocationDialogValueChanged(val newValue: String) : LocationsEvent() {
        override fun perform(model: LocationsModel) = change(
            model.copy(addLocationDialogValue = newValue)
        )
    }

    object AddLocationDialogDismissed : LocationsEvent() {
        override fun perform(model: LocationsModel) = change(
            model.copy(showAddLocationDialog = false)
        )
    }

    object AddLocationDialogConcluded : LocationsEvent() {
        override fun perform(model: LocationsModel): Next<LocationsModel, LocationsEffect> {
            val effects = mutableSetOf<LocationsEffect>().apply {
                add(LocationsEffect.SaveLocation(model.addLocationDialogValue.trim()))
                if (model.locations?.isEmpty() == true) {
                    add(LocationsEffect.SetDefaultLocation(model.addLocationDialogValue.trim()))
                }
            }

            return change(
                model.copy(showAddLocationDialog = false, addLocationDialogValue = ""),
                *effects.toTypedArray()
            )
        }
    }

    data class UserLocationsFetched(val data: Result<UserLocations>) : LocationsEvent() {
        override fun perform(model: LocationsModel) = change(model.copy(data = data))
    }

    data class DefaultLocationSelected(val newDefault: String) : LocationsEvent() {
        override fun perform(model: LocationsModel) =
            dispatch(LocationsEffect.SetDefaultLocation(newDefault))
    }

    data class LocationLongClicked(val location: String) : LocationsEvent() {
        override fun perform(model: LocationsModel) = change(
            model.copy(locationToRemove = location)
        )
    }

    object RemoveLocationDialogDismissed : LocationsEvent() {
        override fun perform(model: LocationsModel) = change(
            model.copy(locationToRemove = null)
        )
    }

    object RemoveLocationRequested : LocationsEvent() {
        override fun perform(model: LocationsModel) =
            change(
                model.copy(locationToRemove = null),
                LocationsEffect.RemoveLocation(model.locationToRemove ?: "")
            )
    }
}
