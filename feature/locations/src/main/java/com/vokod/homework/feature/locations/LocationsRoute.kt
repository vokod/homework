package com.vokod.homework.feature.locations

import androidx.compose.runtime.Composable
import com.bridge.ouroboros.compose.acquireLoop
import com.vokod.homework.feature.locations.ouroboros.LocationsEffect
import com.vokod.homework.feature.locations.ouroboros.locationsInit
import com.vokod.homework.feature.locations.ui.LocationsScreen
import timber.log.Timber


@Composable
fun LocationsRoute(
    navigateBack: () -> Unit,
) {
    val loop = acquireLoop(
        loopInitializer = locationsInit(),
        effectStateFactory = LocationsEffect::State,
        crashHandler = { Timber.e("Error occurred in LocationsEffect: ${it.message}", it) },
        debugLogger = { Timber.d(it) },
        externalEvents = null
    )

    LocationsScreen(
        model = loop.model,
        dispatchEvent = loop::dispatchEvent,
        navigateBack = navigateBack,
    )
}
