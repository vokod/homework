package com.vokod.homework.feature.locations.navigation

import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import com.google.accompanist.navigation.animation.composable
import com.vokod.homework.core.common.NavigationRoutes
import com.vokod.homework.feature.locations.LocationsRoute


fun NavController.navigateToLocations() {
    this.navigate(NavigationRoutes.LOCATIONS)
}

@OptIn(ExperimentalAnimationApi::class)
fun NavGraphBuilder.locationsNavigation(navigateBack: () -> Unit) {
    composable(
        route = NavigationRoutes.LOCATIONS,
        enterTransition = {
            slideIntoContainer(towards = AnimatedContentScope.SlideDirection.Left)
        },
        exitTransition = {
            slideOutOfContainer(towards = AnimatedContentScope.SlideDirection.Right)
        }
    ) { LocationsRoute(navigateBack = navigateBack) }
}
