package com.vokod.homework.feature.locations.ouroboros

import com.bridge.ouroboros.compose.LoopInitializer
import com.bridge.ouroboros.compose.Next

fun locationsInit(): LoopInitializer<LocationsModel, LocationsEffect> = {
    Next.Change(
        LocationsModel(),
        LocationsEffect.FetchUserLocations
    )
}
