package com.vokod.homework.feature.locations.ouroboros

import androidx.compose.runtime.Stable
import com.vokod.homework.core.domain.model.UserLocations


@Stable
data class LocationsModel(
    val data: Result<UserLocations>? = null,
    val showAddLocationDialog: Boolean = false,
    val addLocationDialogValue: String = "",
    val locationToRemove: String? = null,
) {
    val showError: Boolean
        get() = data?.isFailure == true

    val showLoading: Boolean
        get() = data == null

    val locations: Set<String>?
        get() = data?.getOrNull()?.locations

    val defaultLocation: String?
        get() = data?.getOrNull()?.defaultLocation
}
