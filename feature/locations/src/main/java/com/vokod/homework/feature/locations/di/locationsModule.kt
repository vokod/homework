package com.vokod.homework.feature.locations.di

import com.vokod.homework.core.preferences.di.preferenceModule
import com.vokod.homework.repository.di.repositoryModule
import org.koin.dsl.module

val locationsModule = module {
    includes(repositoryModule, preferenceModule)
}
