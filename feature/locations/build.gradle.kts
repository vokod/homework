plugins {
    id("homework.android.feature")
    id("homework.android.library.compose")
}

android { namespace = "com.vokod.homework.feature.locations" }

dependencies {

    implementation(project(":core:common"))
    implementation(project(":core:designsystem"))
    implementation(project(":core:domain"))
    implementation(project(":core:repository"))
    implementation(project(":core:preferences"))

    implementation(libs.accompanist.navigation.animation)
    implementation(libs.androidx.compose.material3)
    implementation(libs.androidx.compose.runtime.livedata)
    implementation(libs.androidx.compose.ui.tooling.preview)
    implementation(libs.androidx.navigation.compose)
    implementation(libs.koin.android)
    implementation(libs.ouroboros)
    implementation(libs.timber)

    testImplementation(libs.kotlinx.coroutines.test)
    testImplementation(libs.mockk.android)
    testImplementation(libs.ouroboros.test)
}
