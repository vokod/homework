package com.vokod.homework.feature.currentweather

import androidx.compose.runtime.Composable
import com.bridge.ouroboros.compose.acquireLoop
import com.vokod.homework.feature.currentweather.ouroboros.CurrentWeatherEffect
import com.vokod.homework.feature.currentweather.ouroboros.currentWeatherInit
import com.vokod.homework.feature.currentweather.ui.CurrentWeatherScreen
import timber.log.Timber


@Composable
fun CurrentWeatherRoute(
    navigateToForecast: () -> Unit,
    navigateToLocations: () -> Unit,
) {
    val loop = acquireLoop(
        loopInitializer = currentWeatherInit(),
        effectStateFactory = CurrentWeatherEffect::State,
        crashHandler = { Timber.e("Error occurred in CurrentWeatherEffect: ${it.message}", it) },
        debugLogger = { Timber.d(it) },
        externalEvents = null
    )

    CurrentWeatherScreen(
        model = loop.model,
        dispatchEvent = loop::dispatchEvent,
        navigateToHourlyForecast = navigateToForecast,
        navigateToLocations = navigateToLocations
    )
}
