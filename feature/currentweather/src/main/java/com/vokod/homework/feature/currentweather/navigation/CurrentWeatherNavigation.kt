package com.vokod.homework.feature.currentweather.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.navigation.NavGraphBuilder
import com.google.accompanist.navigation.animation.composable
import com.vokod.homework.core.common.NavigationRoutes
import com.vokod.homework.feature.currentweather.CurrentWeatherRoute


@OptIn(ExperimentalAnimationApi::class)
fun NavGraphBuilder.currentWeatherNavigation(
    navigateToForecast: () -> Unit,
    navigateToLocations: () -> Unit
) {
    composable(route = NavigationRoutes.CURRENT) {
        CurrentWeatherRoute(
            navigateToForecast = navigateToForecast,
            navigateToLocations = navigateToLocations
        )
    }
}
