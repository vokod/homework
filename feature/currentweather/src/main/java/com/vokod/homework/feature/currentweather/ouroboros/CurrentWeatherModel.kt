package com.vokod.homework.feature.currentweather.ouroboros

import androidx.compose.runtime.Stable
import com.vokod.homework.core.domain.model.Forecast
import com.vokod.homework.core.domain.model.WeatherItem
import com.vokod.homework.feature.currentweather.model.CurrentWeatherData
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter


@Stable
data class CurrentWeatherModel(
    val location: String? = null,
    val data: Result<CurrentWeatherData>? = null,
) {
    val showError: Boolean
        get() = data?.isFailure == true

    val showLoading: Boolean
        get() = data == null && (location == null || location.isNotBlank())

    val showSelectLocationMessage: Boolean
        get() = location?.isBlank() == true

    private val currentWeather: WeatherItem
        get() = data?.getOrThrow()?.currentWeather
            ?: error("currentWeather is null in CurrentWeatherModel")

    val temperature: String
        get() = currentWeather.temp.toInt().toString()

    val humidity: String
        get() = currentWeather.humidity.toString()

    val wind: String
        get() = currentWeather.windSpeed.toInt().toString()

    val weatherDescription: String
        get() = currentWeather.description ?: ""

    val weatherIcon: String
        get() = currentWeather.icon

    private val forecast: Forecast
        get() = data?.getOrThrow()?.forecast ?: error("forecast is null in CurrentWeatherModel")

    fun forecastEpoch(index: Int): Long = forecast.list[index].time

    private fun forecastTime(index: Int) = OffsetDateTime.ofInstant(
        Instant.ofEpochSecond(forecastEpoch(index)),
        ZoneId.systemDefault()
    )

    fun forecastHours(index: Int): String {
        val dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm")
        return forecastTime(index).format(dateTimeFormatter)
    }

    fun forecastTemperature(index: Int): String = forecast.list[index].temp.toInt().toString()

    fun forecastHumidity(index: Int): String = forecast.list[index].humidity.toString()

    fun forecastWind(index: Int): String = forecast.list[index].windSpeed.toInt().toString()

    fun forecastWeatherDescription(index: Int): String =
        forecast.list[index].description ?: ""

    fun forecastWeatherIcon(index: Int): String = forecast.list[index].icon
}
