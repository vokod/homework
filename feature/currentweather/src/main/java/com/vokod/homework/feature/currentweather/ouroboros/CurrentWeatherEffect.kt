package com.vokod.homework.feature.currentweather.ouroboros

import com.bridge.ouroboros.compose.EventConsumer
import com.bridge.ouroboros.compose.ExecutableEffect
import com.vokod.homework.core.common.LongRunningCoroutine
import com.vokod.homework.core.preferences.HomeworkPreferences
import com.vokod.homework.feature.currentweather.usecase.GetCurrentWeatherUsecase
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

sealed class CurrentWeatherEffect :
    ExecutableEffect<CurrentWeatherEvent, CurrentWeatherEffect.State>() {

    data class LoadCurrentWeather(val location: String) : CurrentWeatherEffect() {
        override fun State.perform(emit: EventConsumer<CurrentWeatherEvent>) {
            loadJob?.cancel()
            loadJob = launch {
                emit(CurrentWeatherEvent.CurrentWeatherLoaded(usecase.getCurrentWeather(location)))
            }
        }
    }

    object FetchDefaultLocation : CurrentWeatherEffect() {
        override fun State.perform(emit: EventConsumer<CurrentWeatherEvent>) {
            launch(LongRunningCoroutine) {
                preferences.getDefaultLocation().collect {
                    emit(CurrentWeatherEvent.DefaultLocationFetched(it))
                }
            }
        }
    }

    class State(
        preferences: HomeworkPreferences? = null,
        usecase: GetCurrentWeatherUsecase? = null,
    ) : KoinComponent {
        val preferences: HomeworkPreferences = preferences ?: get()
        val usecase: GetCurrentWeatherUsecase = usecase ?: get()
        var loadJob: Job? = null
    }
}
