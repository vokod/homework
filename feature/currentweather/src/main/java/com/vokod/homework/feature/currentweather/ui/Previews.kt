package com.vokod.homework.feature.currentweather.ui

import androidx.compose.runtime.Composable
import com.vokod.homework.core.designsystem.HomeworkPreview
import com.vokod.homework.core.designsystem.ScreenPreview
import com.vokod.homework.core.domain.model.Fixtures
import com.vokod.homework.feature.currentweather.model.CurrentWeatherData
import com.vokod.homework.feature.currentweather.ouroboros.CurrentWeatherModel


@ScreenPreview
@Composable
fun CurrentWeatherScreenLoadingPreview() {
    HomeworkPreview {
        CurrentWeatherScreen(
            model = CurrentWeatherModel(),
            dispatchEvent = {},
            navigateToHourlyForecast = {},
            navigateToLocations = {}
        )
    }
}

@ScreenPreview
@Composable
fun CurrentWeatherScreenErrorPreview() {
    HomeworkPreview {
        CurrentWeatherScreen(
            model = CurrentWeatherModel(data = Result.failure(Exception())),
            dispatchEvent = {},
            navigateToHourlyForecast = {},
            navigateToLocations = {}
        )
    }
}

@ScreenPreview
@Composable
fun CurrentWeatherScreenContentPreview() {
    HomeworkPreview {
        CurrentWeatherScreen(
            model = CurrentWeatherModel(
                data = Result.success(
                    CurrentWeatherData(
                        currentWeather = Fixtures.weather,
                        forecast = Fixtures.forecast
                    )
                )
            ),
            dispatchEvent = {},
            navigateToHourlyForecast = {},
            navigateToLocations = {}
        )
    }
}
