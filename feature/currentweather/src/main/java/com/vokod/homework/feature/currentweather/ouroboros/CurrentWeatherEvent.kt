package com.vokod.homework.feature.currentweather.ouroboros

import com.bridge.ouroboros.compose.ActionableEvent
import com.bridge.ouroboros.compose.Next
import com.vokod.homework.feature.currentweather.model.CurrentWeatherData


sealed class CurrentWeatherEvent : ActionableEvent<CurrentWeatherModel, CurrentWeatherEffect> {

    data class CurrentWeatherLoaded(val result: Result<CurrentWeatherData>) :
        CurrentWeatherEvent() {
        override fun perform(model: CurrentWeatherModel) =
            change(model.copy(data = result))
    }

    object RetryClicked : CurrentWeatherEvent() {
        override fun perform(model: CurrentWeatherModel) = change(
            model.copy(data = null),
            model.location?.let { CurrentWeatherEffect.LoadCurrentWeather(it) }
                ?: CurrentWeatherEffect.FetchDefaultLocation
        )
    }

    data class DefaultLocationFetched(val location: String) : CurrentWeatherEvent() {
        override fun perform(model: CurrentWeatherModel): Next<CurrentWeatherModel, CurrentWeatherEffect> {
            val effects = mutableSetOf<CurrentWeatherEffect>().apply {
                if (location.isNotBlank()) {
                    add(CurrentWeatherEffect.LoadCurrentWeather(location))
                }
            }
            return change(
                model.copy(data = null, location = location),
                *effects.toTypedArray()
            )
        }
    }
}
