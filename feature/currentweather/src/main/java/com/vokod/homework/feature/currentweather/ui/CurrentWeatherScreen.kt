package com.vokod.homework.feature.currentweather.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FabPosition
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bridge.ouroboros.compose.EventConsumer
import com.vokod.homework.core.common.R
import com.vokod.homework.core.common.weatherIcon
import com.vokod.homework.core.designsystem.component.HomeworkTopAppBar
import com.vokod.homework.core.designsystem.component.RemoteImage
import com.vokod.homework.core.designsystem.component.items.HomeworkCard
import com.vokod.homework.core.designsystem.component.page.FullscreenErrorState
import com.vokod.homework.core.designsystem.component.page.FullscreenLoadingState
import com.vokod.homework.core.designsystem.component.page.ReadableWidthContainer
import com.vokod.homework.core.designsystem.icon.HomeworkIcons
import com.vokod.homework.core.designsystem.toDp
import com.vokod.homework.feature.currentweather.ouroboros.CurrentWeatherEvent
import com.vokod.homework.feature.currentweather.ouroboros.CurrentWeatherModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CurrentWeatherScreen(
    model: CurrentWeatherModel,
    dispatchEvent: EventConsumer<CurrentWeatherEvent>,
    navigateToHourlyForecast: () -> Unit,
    navigateToLocations: () -> Unit,
) {
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(rememberTopAppBarState())

    Scaffold(
        topBar = {
            HomeworkTopAppBar(
                title = {
                    Text(
                        text = model.location ?: stringResource(id = R.string.weather),
                        maxLines = 1,
                        overflow = TextOverflow.Visible
                    )
                },
                actionIcon = HomeworkIcons.Location,
                onActionClick = navigateToLocations,
                scrollBehavior = scrollBehavior
            )
        },
        floatingActionButton = {
            if (model.location?.isNotBlank() == true) {
                FloatingActionButton(onClick = navigateToHourlyForecast) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.padding(horizontal = 16.dp)
                    ) {
                        Icon(
                            painter = HomeworkIcons.Chart.painter,
                            contentDescription = null,
                            tint = MaterialTheme.colorScheme.onSurface
                        )

                        Spacer(modifier = Modifier.size(16.dp))

                        Text(text = stringResource(id = R.string.forecast))
                    }
                }
            }
        },
        floatingActionButtonPosition = FabPosition.Center,
        modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
    )
    { paddingValues ->
        ReadableWidthContainer(
            modifier = Modifier.padding(top = paddingValues.calculateTopPadding())
        ) {
            when {
                model.showLoading -> FullscreenLoadingState()
                model.showError ->
                    FullscreenErrorState { dispatchEvent(CurrentWeatherEvent.RetryClicked) }
                model.showSelectLocationMessage -> Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = stringResource(id = R.string.to_select_a_location_),
                        maxLines = 2,
                        overflow = TextOverflow.Visible,
                        style = MaterialTheme.typography.titleMedium
                    )
                }
                else ->
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .verticalScroll(rememberScrollState()),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Spacer(modifier = Modifier.size(8.dp))

                        Text(
                            text = stringResource(id = R.string.current),
                            maxLines = 1,
                            overflow = TextOverflow.Visible,
                            style = MaterialTheme.typography.labelLarge.copy(
                                color = MaterialTheme.colorScheme.tertiary
                            )
                        )

                        Spacer(modifier = Modifier.size(16.dp))

                        CurrentWeatherCard(
                            currentTemperature = model.temperature,
                            currentWeatherDescription = model.weatherDescription,
                            currentHumidity = model.humidity,
                            currentWind = model.wind,
                            currentWeatherIcon = model.weatherIcon
                        )

                        Spacer(modifier = Modifier.size(32.dp))

                        Text(
                            text = stringResource(R.string.next_24_hours),
                            maxLines = 1,
                            overflow = TextOverflow.Visible,
                            style = MaterialTheme.typography.labelLarge.copy(
                                color = MaterialTheme.colorScheme.tertiary
                            )
                        )

                        Spacer(modifier = Modifier.size(16.dp))

                        LazyRow(
                            modifier = Modifier.fillMaxWidth(),
                            contentPadding = PaddingValues(horizontal = 24.dp),
                            horizontalArrangement = Arrangement.spacedBy(16.dp)
                        ) {
                            items(8, { model.forecastEpoch(it) }) { index ->
                                ForecastCard(
                                    forecastHour = model.forecastHours(index),
                                    forecastTemperature = model.forecastTemperature(index),
                                    forecastWeatherDescription = model.forecastWeatherDescription(
                                        index
                                    ),
                                    forecastHumidity = model.forecastHumidity(index),
                                    forecastWind = model.forecastWind(index),
                                    forecastWeatherIcon = model.forecastWeatherIcon(index)
                                )
                            }
                        }
                    }
            }
        }
    }
}

@Composable
fun CurrentWeatherCard(
    currentTemperature: String,
    currentWeatherDescription: String,
    currentHumidity: String,
    currentWind: String,
    currentWeatherIcon: String
) {
    HomeworkCard(
        modifier = Modifier
            .padding(horizontal = 24.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.size(24.dp))

            Text(
                text = stringResource(id = R.string.c, currentTemperature),
                maxLines = 1,
                overflow = TextOverflow.Visible,
                style = MaterialTheme.typography.displayLarge
            )

            Spacer(modifier = Modifier.size(16.dp))

            Text(
                text = currentWeatherDescription,
                maxLines = 2,
                overflow = TextOverflow.Visible,
                style = MaterialTheme.typography.titleMedium
            )

            Spacer(modifier = Modifier.size(8.dp))

            Row(
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = stringResource(id = R.string.humidity, currentHumidity),
                        maxLines = 1,
                        overflow = TextOverflow.Visible,
                        style = MaterialTheme.typography.headlineSmall
                    )

                    Spacer(modifier = Modifier.size(16.dp))

                    Text(
                        text = stringResource(id = R.string.wind, currentWind),
                        maxLines = 1,
                        overflow = TextOverflow.Visible,
                        style = MaterialTheme.typography.headlineSmall
                    )
                }

                RemoteImage(
                    url = currentWeatherIcon.weatherIcon,
                    modifier = Modifier.size(96.dp)
                )
            }
        }
    }
}

@Composable
fun ForecastCard(
    forecastHour: String,
    forecastTemperature: String,
    forecastWeatherDescription: String,
    forecastHumidity: String,
    forecastWind: String,
    forecastWeatherIcon: String
) {
    HomeworkCard(containerColor = MaterialTheme.colorScheme.tertiaryContainer) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .width(120.sp.toDp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.size(8.dp))

            Text(
                text = forecastHour,
                maxLines = 1,
                overflow = TextOverflow.Visible,
                style = MaterialTheme.typography.labelLarge
            )

            Spacer(modifier = Modifier.size(8.dp))

            Text(
                text = stringResource(id = R.string.c, forecastTemperature),
                maxLines = 1,
                overflow = TextOverflow.Visible,
                style = MaterialTheme.typography.displaySmall
            )

            Spacer(modifier = Modifier.size(4.dp))

            Text(
                text = forecastWeatherDescription,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.titleSmall
            )

            Spacer(modifier = Modifier.size(8.dp))


            Text(
                text = stringResource(id = R.string.humidity, forecastHumidity),
                maxLines = 1,
                overflow = TextOverflow.Visible,
                style = MaterialTheme.typography.labelSmall
            )

            Spacer(modifier = Modifier.size(4.dp))

            Text(
                text = stringResource(id = R.string.wind, forecastWind),
                maxLines = 1,
                overflow = TextOverflow.Visible,
                style = MaterialTheme.typography.labelSmall
            )

            RemoteImage(
                url = forecastWeatherIcon.weatherIcon,
                modifier = Modifier.size(48.dp)
            )
        }
    }
}
