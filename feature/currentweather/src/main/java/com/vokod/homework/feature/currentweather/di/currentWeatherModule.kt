package com.vokod.homework.feature.currentweather.di

import com.vokod.homework.core.preferences.di.preferenceModule
import com.vokod.homework.feature.currentweather.usecase.DefaultGetCurrentWeatherUsecase
import com.vokod.homework.feature.currentweather.usecase.GetCurrentWeatherUsecase
import com.vokod.homework.repository.di.repositoryModule
import org.koin.dsl.module

val currentWeatherModule = module {

    includes(repositoryModule, preferenceModule)

    single<GetCurrentWeatherUsecase> {
        DefaultGetCurrentWeatherUsecase(get())
    }
}
