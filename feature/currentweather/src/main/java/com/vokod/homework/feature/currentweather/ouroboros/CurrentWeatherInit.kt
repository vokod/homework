package com.vokod.homework.feature.currentweather.ouroboros

import com.bridge.ouroboros.compose.LoopInitializer
import com.bridge.ouroboros.compose.Next

fun currentWeatherInit(): LoopInitializer<CurrentWeatherModel, CurrentWeatherEffect> = {
    Next.Change(
        CurrentWeatherModel(),
        CurrentWeatherEffect.FetchDefaultLocation
    )
}
