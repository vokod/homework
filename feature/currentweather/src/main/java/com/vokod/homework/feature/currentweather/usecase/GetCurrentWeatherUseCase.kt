package com.vokod.homework.feature.currentweather.usecase

import com.vokod.homework.feature.currentweather.model.CurrentWeatherData


interface GetCurrentWeatherUsecase {
    suspend fun getCurrentWeather(location: String): Result<CurrentWeatherData>
}

