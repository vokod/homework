package com.vokod.homework.feature.currentweather.usecase

import com.vokod.homework.core.common.resultOf
import com.vokod.homework.feature.currentweather.model.CurrentWeatherData
import com.vokod.homework.repository.WeatherRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope


class DefaultGetCurrentWeatherUsecase(private val repository: WeatherRepository) :
    GetCurrentWeatherUsecase {

    override suspend fun getCurrentWeather(location: String): Result<CurrentWeatherData> =
        resultOf {
            coroutineScope {
                val currentWeatherDeferred = async { repository.getCurrentWeather(location) }
                val forecastDeferred = async { repository.getForecast(location) }

                CurrentWeatherData(currentWeatherDeferred.await(), forecastDeferred.await())
            }
        }
}
