package com.vokod.homework.feature.currentweather.model

import com.vokod.homework.core.domain.model.Forecast
import com.vokod.homework.core.domain.model.WeatherItem

data class CurrentWeatherData(val currentWeather: WeatherItem, val forecast: Forecast)