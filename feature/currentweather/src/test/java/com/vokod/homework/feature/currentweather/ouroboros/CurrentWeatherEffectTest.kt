package com.vokod.homework.feature.currentweather.ouroboros

import com.bridge.ouroboros.compose.test.matches
import com.bridge.ouroboros.compose.test.runWith
import com.vokod.homework.core.domain.model.Fixtures
import com.vokod.homework.core.preferences.HomeworkPreferences
import com.vokod.homework.feature.currentweather.model.CurrentWeatherData
import com.vokod.homework.feature.currentweather.usecase.GetCurrentWeatherUsecase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

class CurrentWeatherEffectTest {

    @MockK
    private lateinit var usecase: GetCurrentWeatherUsecase

    @MockK
    private lateinit var preferences: HomeworkPreferences

    private lateinit var state: CurrentWeatherEffect.State

    private val data = CurrentWeatherData(Fixtures.weather, Fixtures.forecast)

    @Before
    fun before() {
        MockKAnnotations.init(this)
        state = CurrentWeatherEffect.State(preferences, usecase)
    }

    @Test
    fun `LoadCurrentWeather effect`() {
        runTest {
            coEvery { usecase.getCurrentWeather("London") } returns Result.success(data)

            CurrentWeatherEffect.LoadCurrentWeather("London") runWith state matches {
                expectEvents(CurrentWeatherEvent.CurrentWeatherLoaded(Result.success(data)))
            }

            coVerify { usecase.getCurrentWeather("London") }

            confirmVerified(preferences, usecase)
        }
    }

    @Test
    fun `FetchDefaultLocation effect`() {
        runTest {
            coEvery { preferences.getDefaultLocation() } returns flow {
                emit("London")
                emit("Paris")
            }

            CurrentWeatherEffect.FetchDefaultLocation runWith state matches {
                expectEvents(
                    CurrentWeatherEvent.DefaultLocationFetched("London"),
                    CurrentWeatherEvent.DefaultLocationFetched("Paris")
                )
            }

            coVerify { preferences.getDefaultLocation() }

            confirmVerified(preferences, usecase)
        }
    }
}
