package com.vokod.homework.feature.currentweather.ouroboros

import com.bridge.ouroboros.compose.test.matches
import com.bridge.ouroboros.compose.test.receives
import com.vokod.homework.core.domain.model.Fixtures
import com.vokod.homework.feature.currentweather.model.CurrentWeatherData
import org.junit.Test

class CurrentWeatherEventTest {

    private val data = CurrentWeatherData(Fixtures.weather, Fixtures.forecast)

    private val defaultModel = CurrentWeatherModel(
        data = Result.success(data)
    )

    @Test
    fun `RetryClicked event`() {
        defaultModel receives CurrentWeatherEvent.RetryClicked matches {
            shouldHaveModel(defaultModel.copy(data = null))
            shouldHaveEffects(CurrentWeatherEffect.FetchDefaultLocation)
        }
    }

    @Test
    fun `RetryClicked event when location is already known`() {
        val initModel = defaultModel.copy(location = "London")
        initModel receives CurrentWeatherEvent.RetryClicked matches {
            shouldHaveModel(initModel.copy(data = null))
            shouldHaveEffects(CurrentWeatherEffect.LoadCurrentWeather("London"))
        }
    }

    @Test
    fun `CurrentWeatherLoaded event`() {
        val initModel = CurrentWeatherModel()
        initModel receives CurrentWeatherEvent.CurrentWeatherLoaded(Result.success(data)) matches {
            shouldHaveModel(initModel.copy(data = Result.success(data)))
            shouldNotHaveEffects()
        }
    }

    @Test
    fun `DefaultLocationFetched event`() {
        defaultModel receives CurrentWeatherEvent.DefaultLocationFetched("London") matches {
            shouldHaveModel(defaultModel.copy(data = null, location = "London"))
            shouldHaveEffects(CurrentWeatherEffect.LoadCurrentWeather("London"))
        }
    }

    @Test
    fun `DefaultLocationFetched event when location is empty string`() {
        defaultModel receives CurrentWeatherEvent.DefaultLocationFetched("") matches {
            shouldHaveModel(defaultModel.copy(data = null, location = ""))
            shouldNotHaveEffects()
        }
    }

}