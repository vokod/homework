package com.vokod.homework.feature.currentweather.ouroboros

import com.bridge.ouroboros.compose.test.matches
import org.junit.Test

class CurrentWeatherInitTest {

    @Test
    fun init() {
        currentWeatherInit()() matches {
            shouldHaveModel(CurrentWeatherModel())
            shouldHaveEffects(CurrentWeatherEffect.FetchDefaultLocation)
        }
    }
}
