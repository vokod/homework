package com.vokod.homework.feature.currentweather.usecase

import com.vokod.homework.core.domain.model.Fixtures
import com.vokod.homework.feature.currentweather.model.CurrentWeatherData
import com.vokod.homework.repository.WeatherRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DefaultGetCurrentWeatherUsecaseTest {

    @MockK
    private lateinit var repository: WeatherRepository

    private lateinit var sut: DefaultGetCurrentWeatherUsecase

    @Before
    fun before() {
        MockKAnnotations.init(this)
        sut = DefaultGetCurrentWeatherUsecase(repository)
    }

    @Test
    fun success() {
        runTest {
            coEvery { repository.getCurrentWeather(any()) } returns Fixtures.weather

            coEvery { repository.getForecast(any()) } returns Fixtures.forecast

            val expected = CurrentWeatherData(Fixtures.weather, Fixtures.forecast)

            assertEquals(expected, sut.getCurrentWeather("London").getOrThrow())

            coVerify {
                repository.getCurrentWeather("London")
                repository.getForecast("London")
            }

            confirmVerified(repository)
        }
    }

    @Test
    fun fail() {
        runTest {
            coEvery { repository.getCurrentWeather(any()) } returns Fixtures.weather

            coEvery { repository.getForecast(any()) } throws Exception()

            assertTrue(sut.getCurrentWeather("London").isFailure)

            coVerify {
                repository.getCurrentWeather("London")
                repository.getForecast("London")
            }

            confirmVerified(repository)
        }
    }
}