package com.vokod.homework.feature.forecast.ouroboros

import androidx.compose.runtime.Stable
import com.vokod.homework.core.domain.model.Forecast
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneId


@Stable
data class ForecastModel(
    val location: String? = null,
    val data: Result<Forecast>? = null,
) {
    val showError: Boolean
        get() = data?.isFailure == true

    val showLoading: Boolean
        get() = data == null && (location == null || location.isNotBlank())

    val showSelectLocationMessage: Boolean
        get() = location?.isBlank() == true

    val forecast: Forecast
        get() = data?.getOrThrow() ?: error("forecast is null in CurrentWeatherModel")


    fun forecastEpoch(index: Int): Long = forecast.list[index].time

    fun forecastTime(index: Int): OffsetDateTime = OffsetDateTime.ofInstant(
        Instant.ofEpochSecond(forecastEpoch(index)),
        ZoneId.systemDefault()
    )

    fun forecastTemperature(index: Int): String = forecast.list[index].temp.toInt().toString()

    fun forecastHumidity(index: Int): String = forecast.list[index].humidity.toString()

    fun forecastWind(index: Int): String = forecast.list[index].windSpeed.toInt().toString()

    fun forecastWeatherDescription(index: Int): String =
        forecast.list[index].description ?: ""

    fun forecastWeatherIcon(index: Int): String = forecast.list[index].icon
}
