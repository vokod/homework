package com.vokod.homework.feature.forecast.usecase

import com.vokod.homework.core.common.resultOf
import com.vokod.homework.core.domain.model.Forecast
import com.vokod.homework.repository.WeatherRepository


class DefaultGetForecastUsecase(private val repository: WeatherRepository) :
    GetForecastUsecase {

    override suspend fun getForecast(location: String): Result<Forecast> = resultOf {
        repository.getForecast(location)
    }
}
