package com.vokod.homework.feature.forecast.ouroboros

import com.bridge.ouroboros.compose.ActionableEvent
import com.bridge.ouroboros.compose.Next
import com.vokod.homework.core.domain.model.Forecast


sealed class ForecastEvent : ActionableEvent<ForecastModel, ForecastEffect> {

    data class ForecastLoaded(val result: Result<Forecast>) : ForecastEvent() {
        override fun perform(model: ForecastModel) =
            change(model.copy(data = result))
    }

    object RetryClicked : ForecastEvent() {
        override fun perform(model: ForecastModel) = change(
            model.copy(data = null),
            model.location?.let { ForecastEffect.LoadForecast(it) }
                ?: ForecastEffect.FetchDefaultLocation
        )
    }

    data class DefaultLocationFetched(val location: String) : ForecastEvent() {
        override fun perform(model: ForecastModel): Next<ForecastModel, ForecastEffect> {
            val effects = mutableSetOf<ForecastEffect>().apply {
                if (location.isNotBlank()) {
                    add(ForecastEffect.LoadForecast(location))
                }
            }
            return change(
                model.copy(data = null, location = location),
                *effects.toTypedArray()
            )
        }
    }
}
