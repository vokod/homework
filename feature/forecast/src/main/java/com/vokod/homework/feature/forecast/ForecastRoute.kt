package com.vokod.homework.feature.forecast

import androidx.compose.runtime.Composable
import com.bridge.ouroboros.compose.acquireLoop
import com.vokod.homework.feature.forecast.ouroboros.ForecastEffect
import com.vokod.homework.feature.forecast.ouroboros.forecastInit
import com.vokod.homework.feature.forecast.ui.ForecastScreen
import timber.log.Timber


@Composable
fun ForecastRoute(navigateBack: () -> Unit) {
    val loop = acquireLoop(
        loopInitializer = forecastInit(),
        effectStateFactory = ForecastEffect::State,
        crashHandler = { Timber.e("Error occurred in ForecastEffect: ${it.message}", it) },
        debugLogger = { Timber.d(it) },
        externalEvents = null
    )

    ForecastScreen(
        model = loop.model,
        dispatchEvent = loop::dispatchEvent,
        navigateBack = navigateBack,
    )
}
