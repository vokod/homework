package com.vokod.homework.feature.forecast.ui

import android.content.Context
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FabPosition
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.bridge.ouroboros.compose.EventConsumer
import com.vokod.homework.core.common.R
import com.vokod.homework.core.common.weatherIcon
import com.vokod.homework.core.designsystem.component.HomeworkTopAppBar
import com.vokod.homework.core.designsystem.component.RemoteImage
import com.vokod.homework.core.designsystem.component.items.HomeworkCard
import com.vokod.homework.core.designsystem.component.page.FullscreenErrorState
import com.vokod.homework.core.designsystem.component.page.FullscreenLoadingState
import com.vokod.homework.core.designsystem.component.page.ReadableWidthContainer
import com.vokod.homework.core.designsystem.icon.HomeworkIcons
import com.vokod.homework.feature.forecast.ouroboros.ForecastEvent
import com.vokod.homework.feature.forecast.ouroboros.ForecastModel
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ForecastScreen(
    model: ForecastModel,
    dispatchEvent: EventConsumer<ForecastEvent>,
    navigateBack: () -> Unit,
) {
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(rememberTopAppBarState())

    Scaffold(
        topBar = {
            HomeworkTopAppBar(
                title = {
                    Text(
                        text = model.location ?: stringResource(R.string.weather),
                        maxLines = 1,
                        overflow = TextOverflow.Visible
                    )
                },
                scrollBehavior = scrollBehavior,
                navigationIcon = HomeworkIcons.BackArrow,
                onNavigationClick = navigateBack,
                navigationIconContentDescription = stringResource(id = R.string.go_back)
            )
        },
        floatingActionButtonPosition = FabPosition.Center,
        modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
    )
    { paddingValues ->
        ReadableWidthContainer(
            modifier = Modifier.padding(top = paddingValues.calculateTopPadding())
        ) {
            when {
                model.showLoading -> FullscreenLoadingState()
                model.showError ->
                    FullscreenErrorState { dispatchEvent(ForecastEvent.RetryClicked) }
                model.showSelectLocationMessage -> Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = stringResource(R.string.to_select_a_location_),
                        maxLines = 2,
                        overflow = TextOverflow.Visible,
                        style = MaterialTheme.typography.titleMedium
                    )
                }
                else ->
                    LazyColumn(
                        modifier = Modifier.fillMaxSize(),
                        contentPadding = PaddingValues(24.dp),
                        verticalArrangement = Arrangement.spacedBy(24.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        item(key = "header") {
                            Text(
                                text = stringResource(R.string.forecast),
                                maxLines = 1,
                                overflow = TextOverflow.Visible,
                                style = MaterialTheme.typography.labelLarge.copy(
                                    color = MaterialTheme.colorScheme.tertiary
                                )
                            )
                        }

                        items(model.forecast.list.size, { model.forecastEpoch(it) }) { index ->
                            ForecastCard(
                                forecastTime = formatTodayOrFuture(
                                    model.forecastTime(index),
                                    LocalContext.current
                                ),
                                forecastTemperature = model.forecastTemperature(index),
                                forecastWeatherDescription = model.forecastWeatherDescription(
                                    index
                                ),
                                forecastHumidity = model.forecastHumidity(index),
                                forecastWind = model.forecastWind(index),
                                forecastWeatherIcon = model.forecastWeatherIcon(index)
                            )
                        }
                    }
            }
        }
    }
}

@Composable
fun ForecastCard(
    forecastTime: String,
    forecastTemperature: String,
    forecastWeatherDescription: String,
    forecastHumidity: String,
    forecastWind: String,
    forecastWeatherIcon: String
) {
    HomeworkCard(
        modifier = Modifier.fillMaxWidth(),
        containerColor = MaterialTheme.colorScheme.tertiaryContainer
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),

            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = forecastTime,
                maxLines = 1,
                overflow = TextOverflow.Visible,
                style = MaterialTheme.typography.titleMedium
            )

            Spacer(modifier = Modifier.size(8.dp))

            Text(
                text = stringResource(id = R.string.c, forecastTemperature),
                maxLines = 1,
                overflow = TextOverflow.Visible,
                style = MaterialTheme.typography.displaySmall
            )

            Spacer(modifier = Modifier.size(8.dp))

            Text(
                text = forecastWeatherDescription,
                maxLines = 2,
                overflow = TextOverflow.Visible,
                style = MaterialTheme.typography.titleSmall
            )

            Spacer(modifier = Modifier.size(4.dp))

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column {
                    Text(
                        text = stringResource(id = R.string.humidity, forecastHumidity),
                        maxLines = 1,
                        overflow = TextOverflow.Visible,
                        style = MaterialTheme.typography.labelLarge
                    )

                    Spacer(modifier = Modifier.size(16.dp))

                    Text(
                        text = stringResource(id = R.string.wind, forecastWind),
                        maxLines = 1,
                        overflow = TextOverflow.Visible,
                        style = MaterialTheme.typography.labelLarge
                    )
                }

                RemoteImage(
                    url = forecastWeatherIcon.weatherIcon,
                    modifier = Modifier.size(64.dp)
                )
            }
        }
    }
}

private fun formatTodayOrFuture(
    date: OffsetDateTime,
    context: Context,
): String {
    val now = OffsetDateTime.now()
    val days = ChronoUnit.DAYS.between(now, date).toInt()
    val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
    val time = date.format(timeFormatter)
    return if (days < 0) {
        ""
    } else {
        when (days) {
            0 -> context.getString(R.string.today, time)
            1 -> context.getString(R.string.tomorrow, time)
            else -> {
                val dateTimeFormatter = DateTimeFormatter.ofPattern("MMM dd, HH:mm")
                date.format(dateTimeFormatter)
            }
        }
    }
}
