package com.vokod.homework.feature.forecast.di

import com.vokod.homework.core.preferences.di.preferenceModule
import com.vokod.homework.feature.forecast.usecase.DefaultGetForecastUsecase
import com.vokod.homework.feature.forecast.usecase.GetForecastUsecase
import com.vokod.homework.repository.di.repositoryModule
import org.koin.dsl.module

val forecastModule = module {

    includes(repositoryModule, preferenceModule)

    single<GetForecastUsecase> {
        DefaultGetForecastUsecase(get())
    }
}
