package com.vokod.homework.feature.forecast.ui

import androidx.compose.runtime.Composable
import com.vokod.homework.core.designsystem.HomeworkPreview
import com.vokod.homework.core.designsystem.ScreenPreview
import com.vokod.homework.core.domain.model.Fixtures
import com.vokod.homework.feature.forecast.ouroboros.ForecastModel


@ScreenPreview
@Composable
fun ForecastScreenLoadingPreview() {
    HomeworkPreview {
        ForecastScreen(
            model = ForecastModel(),
            dispatchEvent = {},
            navigateBack = {}
        )
    }
}


@ScreenPreview
@Composable
fun ForecastScreenErrorPreview() {
    HomeworkPreview {
        ForecastScreen(
            model = ForecastModel(data = Result.failure(java.lang.Exception())),
            dispatchEvent = {},
            navigateBack = {}
        )
    }
}

@ScreenPreview
@Composable
fun ForecastScreenContentPreview() {
    HomeworkPreview {
        ForecastScreen(
            model = ForecastModel(data = Result.success(Fixtures.forecast)),
            dispatchEvent = {},
            navigateBack = {},
        )
    }
}
