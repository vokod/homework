package com.vokod.homework.feature.forecast.navigation

import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import com.google.accompanist.navigation.animation.composable
import com.vokod.homework.core.common.NavigationRoutes
import com.vokod.homework.feature.forecast.ForecastRoute


fun NavController.navigateToForecast() {
    this.navigate(NavigationRoutes.FORECAST)
}

@OptIn(ExperimentalAnimationApi::class)
fun NavGraphBuilder.forecastNavigation(navigateBack: () -> Unit) {
    composable(
        route = NavigationRoutes.FORECAST,
        enterTransition = {
            slideIntoContainer(towards = AnimatedContentScope.SlideDirection.Up)
        },
        exitTransition = {
            slideOutOfContainer(towards = AnimatedContentScope.SlideDirection.Down)
        }
    ) { ForecastRoute(navigateBack = navigateBack) }
}
