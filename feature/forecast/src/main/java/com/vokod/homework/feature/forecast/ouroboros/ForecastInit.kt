package com.vokod.homework.feature.forecast.ouroboros

import com.bridge.ouroboros.compose.LoopInitializer
import com.bridge.ouroboros.compose.Next

fun forecastInit(): LoopInitializer<ForecastModel, ForecastEffect> = {
    Next.Change(
        ForecastModel(),
        ForecastEffect.FetchDefaultLocation
    )
}
