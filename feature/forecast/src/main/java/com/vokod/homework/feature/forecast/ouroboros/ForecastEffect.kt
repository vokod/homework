package com.vokod.homework.feature.forecast.ouroboros

import com.bridge.ouroboros.compose.EventConsumer
import com.bridge.ouroboros.compose.ExecutableEffect
import com.vokod.homework.core.common.LongRunningCoroutine
import com.vokod.homework.core.preferences.HomeworkPreferences
import com.vokod.homework.feature.forecast.usecase.GetForecastUsecase
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

sealed class ForecastEffect : ExecutableEffect<ForecastEvent, ForecastEffect.State>() {

    data class LoadForecast(val location: String) : ForecastEffect() {
        override fun State.perform(emit: EventConsumer<ForecastEvent>) {
            loadJob?.cancel()
            loadJob = launch {
                emit(ForecastEvent.ForecastLoaded(usecase.getForecast(location)))
            }
        }
    }

    object FetchDefaultLocation : ForecastEffect() {
        override fun State.perform(emit: EventConsumer<ForecastEvent>) {
            launch(LongRunningCoroutine) {
                preferences.getDefaultLocation().collect {
                    emit(ForecastEvent.DefaultLocationFetched(it))
                }
            }
        }
    }

    class State(
        preferences: HomeworkPreferences? = null,
        usecase: GetForecastUsecase? = null,
    ) : KoinComponent {
        val preferences: HomeworkPreferences = preferences ?: get()
        val usecase: GetForecastUsecase = usecase ?: get()
        var loadJob: Job? = null
    }
}
