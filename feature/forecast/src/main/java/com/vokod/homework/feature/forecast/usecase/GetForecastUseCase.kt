package com.vokod.homework.feature.forecast.usecase

import com.vokod.homework.core.domain.model.Forecast


interface GetForecastUsecase {
    suspend fun getForecast(location: String): Result<Forecast>
}
