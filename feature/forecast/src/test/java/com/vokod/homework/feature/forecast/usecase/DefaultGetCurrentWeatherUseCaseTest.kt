package com.vokod.homework.feature.forecast.usecase

import com.vokod.homework.core.domain.model.Fixtures
import com.vokod.homework.repository.WeatherRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DefaultGetForecastUsecaseTest {

    @MockK
    private lateinit var repository: WeatherRepository

    private lateinit var sut: DefaultGetForecastUsecase

    @Before
    fun before() {
        MockKAnnotations.init(this)
        sut = DefaultGetForecastUsecase(repository)
    }

    @Test
    fun success() {
        runTest {
            coEvery { repository.getForecast(any()) } returns Fixtures.forecast

            val expected = Fixtures.forecast

            assertEquals(expected, sut.getForecast("London").getOrThrow())

            coVerify {
                repository.getForecast("London")
                repository.getForecast("London")
            }

            confirmVerified(repository)
        }
    }

    @Test
    fun fail() {
        runTest {
            coEvery { repository.getForecast(any()) } returns Fixtures.forecast

            coEvery { repository.getForecast(any()) } throws Exception()

            assertTrue(sut.getForecast("London").isFailure)

            coVerify { repository.getForecast("London") }

            confirmVerified(repository)
        }
    }
}
