package com.vokod.homework.feature.forecast.ouroboros

import com.bridge.ouroboros.compose.test.matches
import com.bridge.ouroboros.compose.test.receives
import com.vokod.homework.core.domain.model.Fixtures
import org.junit.Test

class ForecastEventTest {

    private val defaultModel = ForecastModel(data = Result.success(Fixtures.forecast))

    @Test
    fun `RetryClicked event`() {
        defaultModel receives ForecastEvent.RetryClicked matches {
            shouldHaveModel(defaultModel.copy(data = null))
            shouldHaveEffects(ForecastEffect.FetchDefaultLocation)
        }
    }

    @Test
    fun `RetryClicked event when location is already known`() {
        val initModel = defaultModel.copy(location = "London")
        initModel receives ForecastEvent.RetryClicked matches {
            shouldHaveModel(initModel.copy(data = null))
            shouldHaveEffects(ForecastEffect.LoadForecast("London"))
        }
    }

    @Test
    fun `ForecastLoaded event`() {
        val initModel = ForecastModel()
        initModel receives ForecastEvent.ForecastLoaded(Result.success(Fixtures.forecast)) matches {
            shouldHaveModel(initModel.copy(data = Result.success(Fixtures.forecast)))
            shouldNotHaveEffects()
        }
    }

    @Test
    fun `DefaultLocationFetched event`() {
        defaultModel receives ForecastEvent.DefaultLocationFetched("London") matches {
            shouldHaveModel(defaultModel.copy(data = null, location = "London"))
            shouldHaveEffects(ForecastEffect.LoadForecast("London"))
        }
    }

    @Test
    fun `DefaultLocationFetched event when location is empty string`() {
        defaultModel receives ForecastEvent.DefaultLocationFetched("") matches {
            shouldHaveModel(defaultModel.copy(data = null, location = ""))
            shouldNotHaveEffects()
        }
    }

}