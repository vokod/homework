package com.vokod.homework.feature.forecast.ouroboros

import com.bridge.ouroboros.compose.test.matches
import org.junit.Test

class ForecastInitTest {

    @Test
    fun init() {
        forecastInit()() matches {
            shouldHaveModel(ForecastModel())
            shouldHaveEffects(ForecastEffect.FetchDefaultLocation)
        }
    }
}
