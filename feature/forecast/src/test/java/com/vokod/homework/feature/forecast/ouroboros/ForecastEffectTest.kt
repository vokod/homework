package com.vokod.homework.feature.forecast.ouroboros

import com.bridge.ouroboros.compose.test.matches
import com.bridge.ouroboros.compose.test.runWith
import com.vokod.homework.core.domain.model.Fixtures
import com.vokod.homework.core.preferences.HomeworkPreferences
import com.vokod.homework.feature.forecast.usecase.GetForecastUsecase
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test

class ForecastEffectTest {

    @MockK
    private lateinit var usecase: GetForecastUsecase

    @MockK
    private lateinit var preferences: HomeworkPreferences

    private lateinit var state: ForecastEffect.State

    private val data = Fixtures.forecast

    @Before
    fun before() {
        MockKAnnotations.init(this)
        state = ForecastEffect.State(preferences, usecase)
    }

    @Test
    fun `LoadForecast effect`() {
        runTest {
            coEvery { usecase.getForecast("London") } returns Result.success(data)

            ForecastEffect.LoadForecast("London") runWith state matches {
                expectEvents(ForecastEvent.ForecastLoaded(Result.success(data)))
            }

            coVerify { usecase.getForecast("London") }

            confirmVerified(preferences, usecase)
        }
    }

    @Test
    fun `FetchDefaultLocation effect`() {
        runTest {
            coEvery { preferences.getDefaultLocation() } returns flow {
                emit("London")
                emit("Paris")
            }

            ForecastEffect.FetchDefaultLocation runWith state matches {
                expectEvents(
                    ForecastEvent.DefaultLocationFetched("London"),
                    ForecastEvent.DefaultLocationFetched("Paris")
                )
            }

            coVerify { preferences.getDefaultLocation() }

            confirmVerified(preferences, usecase)
        }
    }
}
