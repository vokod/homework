import java.io.FileInputStream

buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath("app.cash.paparazzi:paparazzi-gradle-plugin:1.2.0")
    }
}

plugins {
    alias(libs.plugins.android.application) apply false
    id("com.android.library") version "7.4.2" apply false
    id("org.jetbrains.kotlin.android") version "1.8.10" apply false
}

extra {
    val properties = java.util.Properties()
    val propertiesFile = project.rootProject.file("local.properties")

    if (propertiesFile.exists()) {
        properties.load(FileInputStream(propertiesFile.path))
    }

    extra["apikey"] = (System.getenv("OPENWEATHERMAP_API_KEY")
        ?: properties.getProperty("openweathermap.api.key"))
        ?: error("Openweathermap api key is missing")
}

tasks.register("clean").configure {
    delete("build")
}
