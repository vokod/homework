## API key

Openweathermap API kulcs szükséges a fordításhoz, melyet a projekt gyökerében
lévő `local.properties` fájlban kell elhelyezni az `openweathermap.api.key={API_key}` mezőben.

## Felépítés

Az app multimoduláris, ami teljesen overkill egy ilyen méretű projektnél, de ha - a feladatkiirás
szerint - ez egy production alkalmazás lenne, melynek továbbfejleszthetősége fontos, akkor így
csinálnám (valamint ez feltétele a screenshot
teszteknek [Paparazzi](https://github.com/cashapp/paparazzi)-val).

A modulok három szintbe rendezhetőek, app -> feature modulok (az egyes screenek) -> core modulok (
többé kevésbé funkció szerint bontva). A moduláris felépítéshez, illetve a gradle version
catalog/convention plugin-ekhez az inspirációt
a [NowInAndroid](https://github.com/android/nowinandroid) os projekt nyújtotta.

Kiegészítő modul a screenshottest.

## Architectura

DI: [Koin](https://insert-koin.io/)

Data-flow: A core modulok közül a `network` szolgáltatja a remote adatot (Retrofit/Moshi, suspending
function-ök), melyek a `repository` modulban konvertálódnak domain modellekké.

A `preferences` modul szolgáltatja a helyi adatot (PreferenceDataStore, flow-k).

A `feature` modulok a `repository`-ból one-shot adatokat nyernek a usecase-eken keresztül, a
preferenciák állapotát reaktívan nyerik.

A feature modulok a ui állapot/események feldolgozásához
az [ouroboros-compose](https://github.com/get-bridge/ouroboros-compose) nevű
unidirectional-data-flow library-t használják.

UI: Jetpack Compose, Material3, Light/Dark téma

Navigáció: Jetpack Compose Navigation. A feature modulok szub-navigációs-gráfokat tartalmaznak (egy
destinaton-nel).

## Tesztelés

Írtam unit teszteket a repository modulhoz, illetve a feature modulok usecase-eihez,
ouroboros-loopjaihoz. UI teszteket nem írtam, de a screenshottest modul minden feature-höz tartalmaz
screenshot teszteket, melyek
futtatásához [nem szükséges emulátor](https://github.com/cashapp/paparazzi).

Screenshot felvétel: `./gradlew screenshottest:recordPaparazziDebug`
Screenshot ellenőrzés: `./gradlew screenshottest:verifyPaparazziDebug`