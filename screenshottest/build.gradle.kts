plugins {
    id("homework.android.feature")
    id("homework.android.library.compose")
    id("app.cash.paparazzi")
}

android {
    namespace = "com.vokod.homework.screenshottest"
}

dependencies {
    implementation(libs.androidx.compose.material3)
    implementation(libs.androidx.compose.ui.tooling.preview)

    implementation(project(":feature:currentweather"))
    implementation(project(":feature:forecast"))
    implementation(project(":feature:locations"))
    implementation(project(":core:designsystem"))
}
