package com.vokod.homework.screenshottest

import com.vokod.homework.feature.forecast.ui.ForecastScreenContentPreview
import com.vokod.homework.feature.forecast.ui.ForecastScreenErrorPreview
import com.vokod.homework.feature.forecast.ui.ForecastScreenLoadingPreview
import com.vokod.homework.screenshottest.common.ScreenshotTest
import org.junit.Test


class ForecastScreenshotTest : ScreenshotTest() {

    @Test
    fun loading() {
        previewScreenshotTest { ForecastScreenLoadingPreview() }
    }

    @Test
    fun error() {
        previewScreenshotTest { ForecastScreenErrorPreview() }
    }

    @Test
    fun content() {
        previewScreenshotTest { ForecastScreenContentPreview() }
    }
}
