package com.vokod.homework.screenshottest.common

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.platform.LocalInspectionMode
import app.cash.paparazzi.DeviceConfig
import app.cash.paparazzi.Paparazzi
import com.android.ide.common.rendering.api.SessionParams
import com.vokod.homework.core.designsystem.theme.LocalThemeOverride
import com.vokod.homework.core.designsystem.theme.ThemeSettings
import org.junit.Rule
import org.junit.rules.TestName

open class ScreenshotTest {

    @get:Rule
    val normalScreen = Paparazzi(
        renderingMode = SessionParams.RenderingMode.SHRINK,
        deviceConfig = DeviceConfig.PIXEL_5.copy(softButtons = false),
        theme = "android:Theme.Material.Light.NoActionBar"
    )

    @get:Rule
    val testNameRule = TestName()

    fun previewScreenshotTest(
        themeModes: Set<ThemeMode> = setOf(ThemeMode.LIGHT, ThemeMode.DARK),
        content: @Composable () -> Unit,
    ) {
        themeModes.onEach { themeMode ->
            runScreenshotTest(
                paparazzi = normalScreen,
                themeMode = themeMode,
                content = { ThemedPreview(themeMode = themeMode, content = content) }
            )
        }
    }

    private fun runScreenshotTest(
        paparazzi: Paparazzi,
        themeMode: ThemeMode,
        content: @Composable () -> Unit
    ) {
        paparazzi.snapshot(
            name = "${testNameRule.methodName}_${themeMode.name}",
            composable = { content() })
    }

    @Suppress("TestFunctionName")
    @Composable
    internal fun ThemedPreview(themeMode: ThemeMode, content: @Composable () -> Unit) {
        CompositionLocalProvider(
            LocalThemeOverride provides ThemeSettings(darkTheme = themeMode == ThemeMode.DARK)
        ) {
            CompositionLocalProvider(LocalInspectionMode.provides(true)) { content() }
        }
    }
}

enum class ThemeMode { LIGHT, DARK }
