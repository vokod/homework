package com.vokod.homework.screenshottest

import com.vokod.homework.feature.currentweather.ui.CurrentWeatherScreenContentPreview
import com.vokod.homework.feature.currentweather.ui.CurrentWeatherScreenErrorPreview
import com.vokod.homework.feature.currentweather.ui.CurrentWeatherScreenLoadingPreview
import com.vokod.homework.screenshottest.common.ScreenshotTest
import org.junit.Test


class CurrentWeatherScreenshotTest : ScreenshotTest() {

    @Test
    fun loading() {
        previewScreenshotTest { CurrentWeatherScreenLoadingPreview() }
    }

    @Test
    fun error() {
        previewScreenshotTest { CurrentWeatherScreenErrorPreview() }
    }

    @Test
    fun content() {
        previewScreenshotTest { CurrentWeatherScreenContentPreview() }
    }
}
