package com.vokod.homework.screenshottest

import com.vokod.homework.feature.locations.ui.LocationsScreenContentPreview
import com.vokod.homework.feature.locations.ui.LocationsScreenContentPreview_addLocationDialog
import com.vokod.homework.feature.locations.ui.LocationsScreenContentPreview_removeLocationDialog
import com.vokod.homework.feature.locations.ui.LocationsScreenErrorPreview
import com.vokod.homework.feature.locations.ui.LocationsScreenLoadingPreview
import com.vokod.homework.screenshottest.common.ScreenshotTest
import org.junit.Test


class LocationsScreenshotTest : ScreenshotTest() {

    @Test
    fun loading() {
        previewScreenshotTest { LocationsScreenLoadingPreview() }
    }

    @Test
    fun error() {
        previewScreenshotTest { LocationsScreenErrorPreview() }
    }

    @Test
    fun content() {
        previewScreenshotTest { LocationsScreenContentPreview() }
    }

    @Test
    fun addLocationDialog() {
        previewScreenshotTest { LocationsScreenContentPreview_addLocationDialog() }
    }

    @Test
    fun removeLocationDialog() {
        previewScreenshotTest { LocationsScreenContentPreview_removeLocationDialog() }
    }
}
