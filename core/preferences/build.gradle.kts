plugins {
    id("homework.android.library")
}

android { namespace = "com.vokod.homework.core.preferences" }

dependencies {
    implementation(project(":core:domain"))

    implementation(libs.androidx.dataStore.preferences)
    implementation(libs.androidx.dataStore.core)
    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.koin.android)
}