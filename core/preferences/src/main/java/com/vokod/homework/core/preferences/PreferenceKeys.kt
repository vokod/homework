package com.vokod.homework.core.preferences


object PreferenceKeys {
    const val LOCATIONS = "locations"
    const val DEFAULT_LOCATION = "default_location"
}
