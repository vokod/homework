package com.vokod.homework.core.preferences

import com.vokod.homework.core.domain.model.UserLocations
import kotlinx.coroutines.flow.Flow

interface HomeworkPreferences {

    fun getUserLocations(): Flow<UserLocations>

    fun getDefaultLocation(): Flow<String>

    suspend fun addLocation(location: String)

    suspend fun removeLocation(location: String)

    suspend fun setDefaultLocation(location: String)
}
