package com.vokod.homework.core.preferences.di

import com.vokod.homework.core.preferences.DataStoreHomeworkPreferences
import com.vokod.homework.core.preferences.HomeworkPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val preferenceModule = module {
    single<HomeworkPreferences> { DataStoreHomeworkPreferences(androidContext()) }
}
