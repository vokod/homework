package com.vokod.homework.core.preferences

import android.content.Context
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.core.stringSetPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.vokod.homework.core.domain.model.UserLocations
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import java.io.IOException

private val Context.dataStore by preferencesDataStore(name = "homework_preferences")

class DataStoreHomeworkPreferences(context: Context) : HomeworkPreferences {

    private val dataSource = context.dataStore

    override fun getUserLocations(): Flow<UserLocations> {
        val locationsStream =
            getPreference(stringSetPreferencesKey(PreferenceKeys.LOCATIONS), emptySet())
        val defaultLocationStream = getDefaultLocation()

        return combine(
            locationsStream,
            defaultLocationStream,
            ::Pair
        ).map {
            val (locations, defaultLocation) = it
            UserLocations(locations, defaultLocation)
        }
    }

    override fun getDefaultLocation(): Flow<String> =
        getPreference(stringPreferencesKey(PreferenceKeys.DEFAULT_LOCATION), "")

    override suspend fun addLocation(location: String) {
        val locations =
            (getPreferenceOnce(stringSetPreferencesKey(PreferenceKeys.LOCATIONS)) ?: setOf())
                .toMutableSet().apply { add(location) }
        putPreference(stringSetPreferencesKey(PreferenceKeys.LOCATIONS), locations)
    }

    override suspend fun removeLocation(location: String) {
        val locations = (getPreferenceOnce(stringSetPreferencesKey(PreferenceKeys.LOCATIONS))
            ?: setOf()).toMutableSet().apply { remove(location) }
        putPreference(stringSetPreferencesKey(PreferenceKeys.LOCATIONS), locations)

        val defaultLocation =
            getPreferenceOnce(stringPreferencesKey(PreferenceKeys.DEFAULT_LOCATION))

        if (defaultLocation == location) {
            clearDefaultLocation()
        }
    }

    override suspend fun setDefaultLocation(location: String) {
        putPreference(stringPreferencesKey(PreferenceKeys.DEFAULT_LOCATION), location)
    }

    private suspend fun clearDefaultLocation() {
        putPreference(stringPreferencesKey(PreferenceKeys.DEFAULT_LOCATION), null)
    }

    private fun <T> getPreference(key: Preferences.Key<T>, defaultValue: T): Flow<T> =
        dataSource.data.catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map { preferences ->
            val result = preferences[key] ?: defaultValue
            result
        }

    private suspend fun <T> getPreferenceOnce(key: Preferences.Key<T>): T? =
        dataSource.data.first()[key]

    private suspend fun <T> putPreference(key: Preferences.Key<T>, value: T?) {
        dataSource.edit { preferences ->
            value?.let {
                preferences[key] = value
            } ?: preferences.remove(key)
        }
    }
}
