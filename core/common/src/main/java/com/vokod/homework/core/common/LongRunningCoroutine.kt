package com.vokod.homework.core.common

import kotlin.coroutines.CoroutineContext

/**
 * Some coroutines might run for the entire lifecycle of a specific effect handler. Currently,
 * these are mostly .collect {} calls that won't complete until they are cancelled.
 */
object LongRunningCoroutine : CoroutineContext.Element, CoroutineContext.Key<LongRunningCoroutine> {
    override val key get() = this
}
