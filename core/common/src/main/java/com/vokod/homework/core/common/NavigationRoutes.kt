package com.vokod.homework.core.common

object NavigationRoutes {
    const val CURRENT = "current"
    const val LOCATIONS = "locations"
    const val FORECAST = "forecast"
}
