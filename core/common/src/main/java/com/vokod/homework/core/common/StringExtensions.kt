package com.vokod.homework.core.common


val String.weatherIcon: String
    get() = "https://openweathermap.org/img/wn/${this}@4x.png"
