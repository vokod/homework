plugins {
    id("homework.android.library")
}

android {
    @Suppress("UnstableApiUsage")
    buildFeatures { buildConfig = true }

    namespace = "com.vokod.homework.core.common"
}

dependencies {
    implementation(libs.koin.android)
    implementation(libs.kotlinx.coroutines.android)
}
