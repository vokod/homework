plugins { id("homework.android.library") }

android { namespace = "com.vokod.homework.core.repository" }

dependencies {
    implementation(project(":core:domain"))
    implementation(project(":core:network"))

    implementation(libs.koin.android)
    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.koin.android)
    implementation(libs.retrofit.core)

    testImplementation(libs.mockk.android)
    testImplementation(libs.kotlinx.coroutines.test)
}
