package com.vokod.homework.repository


import com.vokod.homework.core.domain.model.Forecast
import com.vokod.homework.core.domain.model.WeatherItem
import com.vokod.homework.core.network.retrofit.api.WeatherApi
import com.vokod.homework.core.network.retrofit.unwrap

class DefaultWeatherRepository(private val api: WeatherApi) : WeatherRepository {

    override suspend fun getCurrentWeather(location: String): WeatherItem =
        WeatherItem from api.getCurrentWeather(location).unwrap()

    override suspend fun getForecast(location: String): Forecast =
        Forecast from api.getForecast(location).unwrap()
}
