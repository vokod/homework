package com.vokod.homework.repository.di

import com.vokod.homework.core.network.di.apiModule
import com.vokod.homework.repository.DefaultWeatherRepository
import com.vokod.homework.repository.WeatherRepository
import org.koin.dsl.module

val repositoryModule = module {

    includes(apiModule)

    single<WeatherRepository> { DefaultWeatherRepository(get()) }
}
