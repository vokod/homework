package com.vokod.homework.repository

import com.vokod.homework.core.domain.model.Forecast
import com.vokod.homework.core.domain.model.WeatherItem

interface WeatherRepository {

    suspend fun getCurrentWeather(location: String): WeatherItem

    suspend fun getForecast(location: String): Forecast
}
