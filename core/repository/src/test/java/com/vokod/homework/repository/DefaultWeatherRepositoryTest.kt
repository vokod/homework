package com.vokod.homework.repository

import com.vokod.homework.core.domain.model.Forecast
import com.vokod.homework.core.domain.model.WeatherItem
import com.vokod.homework.core.network.model.NetworkWeatherItem
import com.vokod.homework.core.network.model.NetworkForecast
import com.vokod.homework.core.network.model.NetworkMainWeatherData
import com.vokod.homework.core.network.model.NetworkWeather
import com.vokod.homework.core.network.model.NetworkWind
import com.vokod.homework.core.network.retrofit.ExceptionWithCustomKeys
import com.vokod.homework.core.network.retrofit.api.WeatherApi
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import kotlin.test.assertEquals


internal class DefaultWeatherRepositoryTest {

    @MockK
    private lateinit var api: WeatherApi

    private lateinit var sut: DefaultWeatherRepository

    private val dt = System.currentTimeMillis() / 1000

    @Before
    fun before() {
        MockKAnnotations.init(this)
        sut = DefaultWeatherRepository(api)
    }

    @Test
    fun getCurrentWeather() {
        val apiResult = NetworkWeatherItem(
            time = dt,
            weather = listOf(NetworkWeather(description = null, icon = "")),
            main = NetworkMainWeatherData(
                temp = 0f,
                pressure = 0,
                humidity = 0,
            ),
            wind = NetworkWind(0f, 0),
        )

        val expected = WeatherItem(
            description = null,
            icon = "",
            temp = 0f,
            humidity = 0,
            time = dt,
            windSpeed = 0f
        )
        runTest {
            coEvery { api.getCurrentWeather("London", any()) } returns Response.success(apiResult)

            val result = sut.getCurrentWeather("London")

            assertEquals(expected, result)

            coVerify { api.getCurrentWeather("London", any()) }

            confirmVerified(api)
        }
    }

    @Test(expected = ExceptionWithCustomKeys::class)
    fun getCurrentWeather_fails() {
        runTest {
            coEvery { api.getCurrentWeather("London", any()) } returns Response.error(
                400,
                mockk(relaxed = true)
            )

            sut.getCurrentWeather("London")

            coVerify { api.getCurrentWeather("London", any()) }

            confirmVerified(api)
        }
    }

    @Test
    fun getForecast() {
        val apiResult = NetworkForecast(emptyList())

        val expected = Forecast(emptyList())

        runTest {
            coEvery { api.getForecast("London", any()) } returns Response.success(apiResult)

            val result = sut.getForecast("London")

            assertEquals(expected, result)

            coVerify { api.getForecast("London", any()) }

            confirmVerified(api)
        }
    }

    @Test(expected = ExceptionWithCustomKeys::class)
    fun getForecast_fails() {
        runTest {
            coEvery { api.getForecast("London", any()) } returns Response.error(
                400,
                mockk(relaxed = true)
            )

            sut.getForecast("London")

            coVerify { api.getForecast("London", any()) }

            confirmVerified(api)
        }
    }
}
