package com.vokod.homework.core.designsystem.component.page

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.vokod.homework.core.designsystem.HomeworkPreview
import com.vokod.homework.core.designsystem.ScreenPreview

@Composable
fun FullscreenLoadingState(modifier: Modifier = Modifier) {
    Box(
        modifier = modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}

@ScreenPreview
@Composable
fun FullScreenLoadingStatePreview() {
    HomeworkPreview { FullscreenLoadingState() }
}
