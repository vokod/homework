package com.vokod.homework.core.designsystem.component.page

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.vokod.homework.core.common.R
import com.vokod.homework.core.designsystem.HomeworkPreview
import com.vokod.homework.core.designsystem.ScreenPreview

@Composable
fun FullscreenErrorState(
    modifier: Modifier = Modifier,
    onRetryClick: () -> Unit = {}
) {
    Box(
        modifier = modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(stringResource(R.string.something_went_wrong))
            Spacer(modifier = Modifier.height(16.dp))
            Button(onClick = onRetryClick, modifier = Modifier.defaultMinSize(minHeight = 48.dp)) {
                Text(stringResource(R.string.retry))
            }
        }
    }
}

@ScreenPreview
@Composable
fun FullScreenErrorStatePreview() {
    HomeworkPreview { FullscreenErrorState {} }
}
