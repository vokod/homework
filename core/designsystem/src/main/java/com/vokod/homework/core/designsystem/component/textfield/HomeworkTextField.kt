package com.vokod.homework.core.designsystem.component.textfield

import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import com.vokod.homework.core.designsystem.icon.HomeworkIcons
import com.vokod.homework.core.designsystem.theme.ContentAlpha

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeworkTextField(
    value: String?,
    modifier: Modifier = Modifier,
    placeholder: String?,
    contentDescription: String = "",
    enabled: Boolean = true,
    focusManager: FocusManager = LocalFocusManager.current,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default.copy(
        keyboardType = KeyboardType.Text,
        imeAction = ImeAction.Done
    ),
    keyboardActions: KeyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
    trailingIcon: @Composable (() -> Unit)? = null,
    onValueChanged: (String) -> Unit
) {
    OutlinedTextField(
        maxLines = 1,
        modifier = modifier
            .semantics { this.contentDescription = contentDescription },
        value = value ?: "",
        placeholder = placeholder?.let { { Text(text = placeholder) } },
        onValueChange = onValueChanged,
        enabled = enabled,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        trailingIcon = trailingIcon,
        singleLine = true,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            placeholderColor = MaterialTheme.colorScheme.onBackground.copy(
                alpha = ContentAlpha.disabled
            )
        ),
        leadingIcon = {
            Icon(
                painter = HomeworkIcons.Location.painter,
                tint = MaterialTheme.colorScheme.primary,
                modifier = Modifier.alpha(.6f),
                contentDescription = null
            )
        }
    )
}
