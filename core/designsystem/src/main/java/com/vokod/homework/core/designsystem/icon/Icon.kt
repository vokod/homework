package com.vokod.homework.core.designsystem.icon

import androidx.annotation.DrawableRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Add
import androidx.compose.material.icons.outlined.ArrowBack
import androidx.compose.material.icons.outlined.Check
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material.icons.outlined.Edit
import androidx.compose.material.icons.outlined.Insights
import androidx.compose.material.icons.outlined.LocationOn
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.vectorResource


/**
 * Homework icons. Material icons are [ImageVector]s, custom icons are drawable resource IDs.
 */
object HomeworkIcons {
    val Location = Icon.ImageVectorIcon(Icons.Outlined.LocationOn)
    val BackArrow = Icon.ImageVectorIcon(Icons.Outlined.ArrowBack)
    val Chart = Icon.ImageVectorIcon(Icons.Outlined.Insights)
    val Edit = Icon.ImageVectorIcon(Icons.Outlined.Edit)
    val Check = Icon.ImageVectorIcon(Icons.Outlined.Check)
    val Plus = Icon.ImageVectorIcon(Icons.Outlined.Add)
    val Trash = Icon.ImageVectorIcon(Icons.Outlined.Delete)
}

sealed class Icon {

    abstract val painter: Painter
        @Composable get

    data class ImageVectorIcon(val imageVector: ImageVector) : Icon() {

        override val painter: Painter
            @Composable get() = rememberVectorPainter(image = imageVector)
    }

    data class DrawableResourceIcon(@DrawableRes val id: Int) : Icon() {

        override val painter
            @Composable get() = rememberVectorPainter(image = ImageVector.vectorResource(id = id))
    }
}
