package com.vokod.homework.core.designsystem.component.dialog

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.vokod.homework.core.common.R
import com.vokod.homework.core.designsystem.icon.HomeworkIcons

@Composable
fun ConfirmationDialog(
    title: String?,
    message: String?,
    onConfirmButtonClick: () -> Unit,
    onDismissButtonClick: () -> Unit = {},
    confirmButtonText: String = stringResource(R.string.ok),
    dismissButtonText: String = stringResource(R.string.cancel),
) {
    AlertDialog(
        icon = {
            Icon(painter = HomeworkIcons.Trash.painter, contentDescription = null)
        },
        onDismissRequest = onDismissButtonClick,
        title = { title?.let { Text(text = it) } },
        text = { message?.let { Text(text = it) } },
        confirmButton = {
            TextButton(onClick = onConfirmButtonClick) {
                Text(text = confirmButtonText)
            }
        },
        dismissButton = {
            TextButton(onClick = onDismissButtonClick) {
                Text(text = dismissButtonText)
            }
        }
    )
}
