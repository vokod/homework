package com.vokod.homework.core.designsystem.component.page

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import com.vokod.homework.core.designsystem.readableWidth

@Composable
fun ReadableWidthContainer(
    modifier: Modifier = Modifier,
    contentAlignment: Alignment = Alignment.TopCenter,
    content: @Composable () -> Unit = {}
) {
    Box(modifier = modifier.fillMaxSize(), contentAlignment = contentAlignment) {
        val context = LocalContext.current
        Box(modifier = Modifier.fillMaxWidth(context.readableWidth)) { content() }
    }
}
