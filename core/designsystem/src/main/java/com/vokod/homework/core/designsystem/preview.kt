package com.vokod.homework.core.designsystem

import android.content.res.Configuration
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import com.vokod.homework.core.designsystem.theme.HomeworkTheme

@Preview(name = "light theme")
@Preview(name = "dark theme", uiMode = Configuration.UI_MODE_NIGHT_YES)
@Preview(name = "largest font", fontScale = 2f)
annotation class ComponentPreview

@Preview(name = "light theme")
@Preview(name = "dark theme", uiMode = Configuration.UI_MODE_NIGHT_YES)
@Preview(name = "largest font", fontScale = 2f)
@Preview(name = "landscape", device = Devices.AUTOMOTIVE_1024p, widthDp = 720, heightDp = 360)
@Preview(name = "tablet", device = Devices.NEXUS_10)
annotation class ScreenPreview

@Composable
fun HomeworkPreview(content: @Composable () -> Unit) {
    HomeworkTheme { Surface { content() } }
}
