package com.vokod.homework.core.designsystem

import android.content.Context
import android.util.TypedValue
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit

val Context.readableWidth: Float
    get() {
        val outValue = TypedValue()
        resources.getValue(R.dimen.readable_width, outValue, true)
        return outValue.float
    }

val TextUnit.toDp: Dp
    @Composable
    get() = with(LocalDensity.current) { toDp() }
