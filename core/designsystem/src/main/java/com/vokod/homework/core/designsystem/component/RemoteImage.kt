package com.vokod.homework.core.designsystem.component

import androidx.compose.foundation.Image
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cloud
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalInspectionMode
import coil.compose.AsyncImage

@Composable
fun RemoteImage(
    url: String?,
    modifier: Modifier = Modifier,
    contentDescription: String? = null,
    placeholderPainter: Painter? = null,
    contentScale: ContentScale = ContentScale.Crop
) {
    if (LocalInspectionMode.current && placeholderPainter == null) {
        Image(
            painter = rememberVectorPainter(image = Icons.Default.Cloud),
            contentDescription = contentDescription,
            modifier = modifier,
            contentScale = contentScale,
        )
    } else {
        AsyncImage(
            modifier = modifier,
            contentScale = contentScale,
            model = url,
            contentDescription = null,
            error = placeholderPainter,
            placeholder = placeholderPainter
        )
    }
}
