plugins {
    id("homework.android.library")
    id("homework.android.library.compose")
}

android {
    lint {
        @Suppress("UnstableApiUsage")
        checkDependencies = true
    }
    namespace = "com.vokod.homework.core.designsystem"
}

dependencies {
    implementation(project(":core:common"))

    implementation(libs.accompanist.systemuicontroller)
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.compose.foundation)
    implementation(libs.androidx.compose.foundation.layout)
    implementation(libs.androidx.compose.material.iconsExtended)
    implementation(libs.androidx.compose.material3)
    implementation(libs.androidx.compose.ui.tooling.preview)
    implementation(libs.androidx.compose.ui.util)
    implementation(libs.androidx.compose.runtime)
    implementation(libs.coil.kt.compose)
    implementation(libs.coil.kt)

    debugImplementation(libs.androidx.compose.ui.tooling)
}