package com.vokod.homework.core.domain.model

data class UserLocations(val locations: Set<String>, val defaultLocation: String?)
