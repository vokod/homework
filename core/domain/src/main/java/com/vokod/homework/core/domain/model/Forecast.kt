package com.vokod.homework.core.domain.model

import com.vokod.homework.core.network.model.NetworkForecast

data class Forecast(val list: List<WeatherItem>) {

    companion object {
        infix fun from(input: NetworkForecast) =
            Forecast(input.list.map { WeatherItem from it })
    }
}
