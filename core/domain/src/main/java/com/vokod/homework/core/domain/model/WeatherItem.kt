package com.vokod.homework.core.domain.model

import com.vokod.homework.core.network.model.NetworkWeatherItem

data class WeatherItem(
    val time: Long,
    val description: String? = null,
    val icon: String,
    val temp: Float,
    val humidity: Int,
    val windSpeed: Float,
) {

    companion object {
        infix fun from(input: NetworkWeatherItem) =
            WeatherItem(
                time = input.time,
                description = input.weather[0].description,
                icon = input.weather[0].icon,
                temp = input.main.temp,
                humidity = input.main.humidity,
                windSpeed = input.wind.speed
            )
    }
}
