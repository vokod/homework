package com.vokod.homework.core.domain.model

import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneOffset

object Fixtures {
    private val midnight: LocalDateTime = LocalDateTime.now().with(LocalTime.MIDNIGHT)
    private val epochSeconds = midnight.atZone(ZoneOffset.UTC).toEpochSecond()
    private const val SECONDS_IN_HOUR = 60 * 60
    val weather = WeatherItem(
        time = epochSeconds,
        description = "Light drizzle",
        icon = "d09",
        humidity = 100,
        temp = 23f,
        windSpeed = 25f
    )
    val forecast = generateForecast()

    private fun generateForecast(
        num: Int = 8,
        timeDifference: Int = SECONDS_IN_HOUR * 3
    ): Forecast = Forecast(
        buildList {
            repeat(num) { index ->
                add(weather.copy(time = epochSeconds + timeDifference * index))
            }
        }
    )
}