plugins {
    id("homework.android.library")
}

android { namespace = "com.vokod.homework.core.domain" }

dependencies {
    implementation(project(":core:network"))

    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.koin.android)
    implementation(libs.timber)
}
