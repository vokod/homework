plugins {
    id("homework.android.library")
    alias(libs.plugins.ksp)
}

android {
    @Suppress("UnstableApiUsage")
    buildFeatures { buildConfig = true }
    namespace = "com.vokod.homework.core.network"

    defaultConfig {
        val openweathermapApiKey = rootProject.extra["apikey"]
        buildConfigField("String", "OPENWEATHERMAP_API_KEY", "\"$openweathermapApiKey\"")
    }
}

dependencies {
    implementation(project(":core:common"))

    implementation(libs.koin.android)
    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.moshi)
    implementation(libs.moshi.adapters)
    implementation(libs.moshi.kotlin)
    implementation(libs.okhttp.logging)
    implementation(libs.retrofit.core)
    implementation(libs.retrofit.moshi)
    implementation(libs.timber)
    ksp(libs.moshi.codegen)
}
