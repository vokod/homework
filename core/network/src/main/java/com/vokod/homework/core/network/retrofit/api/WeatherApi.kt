package com.vokod.homework.core.network.retrofit.api

import com.vokod.homework.core.network.BuildConfig
import com.vokod.homework.core.network.model.NetworkWeatherItem
import com.vokod.homework.core.network.model.NetworkForecast
import com.vokod.homework.core.network.model.Units
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.Locale

interface WeatherApi {

    @GET("weather")
    suspend fun getCurrentWeather(
        @Query("q") location: String,
        @Query("lang") lang: String = Locale.getDefault().language,
        @Query("units") unit: Units = Units.METRIC,
        @Query("appid") apiKey: String = BuildConfig.OPENWEATHERMAP_API_KEY
    ): Response<NetworkWeatherItem>

    @GET("forecast")
    suspend fun getForecast(
        @Query("q") location: String,
        @Query("lang") lang: String = Locale.getDefault().language,
        @Query("units") unit: String = "metric",
        @Query("appid") apiKey: String = BuildConfig.OPENWEATHERMAP_API_KEY
    ): Response<NetworkForecast>
}
