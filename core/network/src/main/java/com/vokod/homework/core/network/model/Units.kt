package com.vokod.homework.core.network.model

import com.squareup.moshi.Json

enum class Units {

    @Json(name = "metric")
    METRIC,

    @Json(name = "imperial")
    IMPERIAL
}
