package com.vokod.homework.core.network.model

data class NetworkForecast(val list: List<NetworkWeatherItem>)
