package com.vokod.homework.core.network.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.EnumJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.vokod.homework.core.network.BuildConfig
import com.vokod.homework.core.network.model.Units
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import java.io.File
import java.util.concurrent.TimeUnit

val networkModule = module {

    single {
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .add(
                Units::class.java,
                EnumJsonAdapter.create(Units::class.java)
            )
            .build()
    }

    single<Interceptor> {
        HttpLoggingInterceptor().apply {
            level =
                if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor.Level.BODY
                } else {
                    HttpLoggingInterceptor.Level.NONE
                }
        }
    }

    factory {
        OkHttpClient.Builder()
            .cache(
                Cache(
                    directory = File(androidContext().cacheDir, "http_cache"),
                    maxSize = 50L * 1024L * 1024L // 50 MiB
                )
            )
            .addInterceptor(get<Interceptor>())
            .readTimeout(
                getProperty(
                    "http_read_timeout_seconds",
                    MILLISECONDS_IN_MINUTE.toString()
                ).toLong(),
                TimeUnit.MILLISECONDS
            )
    }

    single {
        get<OkHttpClient.Builder>().build()
    }
}

private const val MILLISECONDS_IN_MINUTE = 1000 * 60
