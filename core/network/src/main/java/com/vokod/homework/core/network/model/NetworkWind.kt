package com.vokod.homework.core.network.model

import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class NetworkWind(val speed: Float, val deg: Int)
