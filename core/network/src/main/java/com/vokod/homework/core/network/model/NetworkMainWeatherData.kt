package com.vokod.homework.core.network.model

import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class NetworkMainWeatherData(val temp: Float, val pressure: Int, val humidity: Int)
