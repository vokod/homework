package com.vokod.homework.core.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NetworkWeatherItem(
    @Json(name = "dt")
    val time: Long = System.currentTimeMillis() / 1000,
    val weather: List<NetworkWeather>,
    val main: NetworkMainWeatherData,
    val wind: NetworkWind
)
