package com.vokod.homework.core.network.retrofit

import retrofit2.Response


fun <T> Response<T>.unwrap(): T {
    return when {
        isSuccessful -> body() ?: throw missingBodyException()
        else -> {
            throw requestFailure()
        }
    }
}

private fun <T> Response<T>.requestFailure(): RequestFailure {
    val message =
        "Request failed with code ${code()} ${message()} on ${this.raw().request.url} and body (${errorBody()?.string()})"
    return RequestFailure(message)
}

private fun <T> Response<T>.missingBodyException(): MissingBodyException =
    MissingBodyException("on ${this.raw().request.url}")

class MissingBodyException(message: String? = null, keys: Map<String, Any> = emptyMap()) :
    ExceptionWithCustomKeys(message, keys = keys)

class RequestFailure(
    message: String? = null,
    keys: Map<String, Any> = emptyMap(),
    cause: Throwable? = null
) : ExceptionWithCustomKeys(message, keys = keys, throwable = cause)

abstract class ExceptionWithCustomKeys(
    message: String? = null,
    throwable: Throwable? = null,
    val keys: Map<String, Any> = emptyMap()
) : Exception(message, throwable)
