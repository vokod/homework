package com.vokod.homework.core.network.di

import com.vokod.homework.core.network.retrofit.api.WeatherApi
import org.koin.core.scope.Scope
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val apiModule = module {

    includes(networkModule)

    val baseUrl = "https://api.openweathermap.org/data/2.5/"

    single { unauthenticatedApi<WeatherApi>(baseUrl) }
}

inline fun <reified T> Scope.unauthenticatedApi(baseUrl: String): T {
    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(get())
        .addConverterFactory(MoshiConverterFactory.create(get()))
        .build()
        .create(T::class.java)
}
