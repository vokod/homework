package com.vokod.homework.core.network.model

import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class NetworkWeather(val description: String? = null, val icon: String)
